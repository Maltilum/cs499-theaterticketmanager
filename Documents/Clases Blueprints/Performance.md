#Perfomance

##Private Fields

###performanceValue – A uniquely assigned ID for the model.performance

•	String

###model.performance A list of the performances in a season

•	ArrayList

###date – displays the month, day, and year of the model.performance to occur

•	int month

•	int day

•	int year

###time – display the time the model.performance is to occur

•	int hour

•	int minute

•	String timeSetting – (AM or PM)

###model.performance – name of model.performance

•	String

###name_Performance_Company

•	String

###name_Of_Actors:

•	String firstName

•	String lastName

###status – displays whether canceled or ongoing

•	Boolean

##Public methods

###model.performance – The constructor of the model.performance object

Parameters:

•	String PerformanceID – unique ID for the model.performance

###getPerformanceID returns the String value of the model.performance

###getPerfomance returns the ArrayList containing the specific model.performance requested.

###addPerformance Adds the specified model.performance to the list of model.performance in the array

Parameter:

•	Performance model.performance – model.performance to be added

###removePerformace removes the model.performance from the list if the model.performance is canceled.

Parameter:

•	String performanceID: unique ID of the model.performance to be removed.

###getPerformanceStatus – returns the status of the model.performance

Parameters:

•	String status: tell if the show is ongoing or cancelled.