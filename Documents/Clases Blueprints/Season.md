#Season

##Private Fields

###seasonAvailability

•	Boolean

##SeasonArray

•	ArrayList -- list of the productions

##Public Methods

###Season – Constructor for the season

##Connect to the Production class

###getProductionName returns to the array list of productions (Returns ArrayList)

###addProduction – Adds a model.performance.production to the list of productions

Parameters:

•	Production productions – model.performance.production to be added.

###removeProduction – removes a model.performance.production if the model.performance.production is cancelled

Parameter:

•	String productionID: the unique ID of the model.performance.production to be removed.