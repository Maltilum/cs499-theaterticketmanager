#Facility

##Private Fields

###facilityName
The name of this facility
* String

###capacity
The maximum number of seats at this venue
* int

###sections
The list of sections in this facility
* ArrayList < Section >

##Public Methods

###Facility
The constructor for this facility

Parameters:
* String name: The name of this facility

###getCapacity
Returns the int value of capacity

###setCapacity
Sets the maximum capacity of the venue

Parameters:
* int capacity: the value to update capacity

###getSections
Returns an ArrayList< Section > of all of the sections in the facility

###addSection
Adds a section to the list of sections

Parameters:
* Section section: the section to be added

###removeSection
Removes the section with the specified Id

Parameters:
* String sectionId: the unique Id of the section to be removed