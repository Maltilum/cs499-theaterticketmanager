#Section

##Private Fields

###sectionValue
A uniquely assigned ID for the section

* String

###sectionAvailability
Some sections may not be available for certain performances

* Boolean

###rows
A List of the Rows of rows in the section 

* ArrayList< Row >

###sectionPrice
The price of the rows in the section

* double

##Public Methods

###Section
The constructor for a new Section object

Parameters:
* String Id: the unique Id for the section

###getId
returns the String value of this section's unique Id

###getRows
returns the ArrayList< Row > containing the rows in the section

###addRow
Adds the specified row to the list of rows in the section

Parameters:
* Row row: the row to be added

###removeRow
Removes a specified row from the list.

Parameters are dependent on the unique identifier for a row.

###getSectionPrice
Returns the double value of the  price for rows in this section

###setSectionPrice
Sets the price for this section

Parameters:
* double price: the price that the rows in the section will cost

###getAvailability
Returns the boolean value of sectionAvailability

###setAvailability
Changes the value of sectionAvailability

Parameters:
* boolean availability: the new availability value



