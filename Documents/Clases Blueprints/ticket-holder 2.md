# Ticket Holder

## Private Fields

### ticket_value

The amount that they spent on their ticket. This is nessisary for point 18.

* Floating point

### disabilities

Their disability information and any special needs they might have.

* A list that stores ENUM values; one for an array of common disabilites.
  * Allow user to add ENUM values if possible.
* An Other option that allows the user to enter a different disability.

### notes

This is where any other notes can be stored.

* String

## Public Functions

### get_disabilities

return the value of disabilites

### add_disability

Should allow a disability to be added to the list.

### remove_disability

### get_ticket_value

return ticket value

### get_notes

Get the notes
