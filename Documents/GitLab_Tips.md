# Gitlab Tips

These are just some tips for using GitLab. Nothing big, just some usefull features I wanted to point out.

## Merge Requests

### Assigneess

When you create a merge request, you can "assign" other users to that request as assignees.

This makes the merge request show up in their to-do feed on GitLab.

### Approval Rules

When you create a merge request you can set a number of people that need to approve of the merge request before it can be merged.

It's a good idea to set this to at leats one.
