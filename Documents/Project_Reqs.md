# CS 499 Senior Project - Theater Ticket Manager

## Rationale

In the city of Huntsville, there are two public performing theater
venues, the Civic Center Playhouse with a maximum of about 500 seats
and the Civic Center Concert Hall with approximately 2000 seats
available for performances. Several non-profit organizations book
these halls on a regular basis and sell tickets. Managing the tickets
and their sales requires much effort on the part of volunteers and
ways are sought to ease their workload.

One of the ultimate embarrassments in these groups is for any two
people to ever be assigned the same seat. Ushers and house managers
are very uncomfortable and unhappy about it.

These organizations have determined that they would like to acquire
custom software to satisfy three purposes:

1.  The software will make it easy for volunteers to manage the
    ticketing process.

2.  A common software package shared by two or more groups makes it
    easier for volunteers to help other groups without having to learn
    new procedures.

3.  Changes in Civic Center seating or policies may be quickly
    incorporated into all organization\'s procedure.

The organizations have agreed that they will sponsor the development
of the software. They have developed this project description as their
statement of need for the team to develop. Some isolated situations
may require manual intervention beyond that which an automated system
could reasonably provide. Consideration must be given to exceptional
situations such that the system can still perform its functions
without hampering individual needs. Theater groups will generally
appoint one person each year to act as supervisor of their account in
the ticketing system

**Features**

The features listed below shall be included in the software.

1.  There will be some facility for new users to become acquainted with
    the system. Users (even experienced ones) cannot be expected to be
    computer-literate and must learn the system quickly.

2.  Since different groups will share the software, most policies and
    prices must be made configurable for each group. Such configuration
    must be easy to create and change for a new group and still be
    robust during use by a number of users.

3.  Tickets may be purchased by cash, check, or credit card. Prices may
    vary by section and for special groups.

4.  Season ticket holders\' names addresses and seat assignments are
    recorded.

5.  Tickets are generally identified by section, row and seat
    indicators. There may be special indications for disabled persons.

6.  Users shall be able to select seats for a model.performance by clicking on
    those seats in a display showing all available seats for either the
    Playhouse or Concert Hall.

7.  All information must be able to be displayed for a human user to
    view.

8.  Displays should be default driven; e.g., tickets are usually
    purchased in pairs.

9.  There will be appropriate visible mechanisms for initiating all
    operations.

10. Users will receive timely feedback displaying the result of any
    operation.

11. Each season ticket holder\'s name, address and seat assignments will
    be retained by the system for an indefinite period.

12. Each season ticket holder\'s information can be edited and/or
    exported.

13. Seats will be marked as they are sold to prevent their being sold
    again.

14. A season ticker holder\'s name address and seat number will be
    tracked by the system from year to year to facilitate renewals.
    Season ticket holders often renew their tickets with the same seat
    assignment.

15. The list of season ticket holders from any year can be maintained as
    needed by changing names, addresses, phone numbers, etc.

16. The system will be able to record individual reservations as they
    are received by the ticket manager, and their paid/unpaid status
    recorded. When the tickets are picked up and/or paid for, the system
    can keep track of that.

17. A ticket to an individual model.performance can be purchased in advance or
    at the door, and the system will keep this information.

18. A ticket may be exchanged for another ticket of equal or differing
    value. The new ticket may be for the same or differing show or
    model.performance.

19. The address list and ticket assignment list may be exported for
    other programs to use. The form of the exported information will be
    customizable by a user or by the organization.

20. The address list and ticket assignment list may be imported from
    other programs. The form of the imported information will be
    customizable by a user or by the organization, obviously subject to
    the presence or absence of any particular data item in the other
    program.

**Constraints**

System must be portable.

Users may run the program on a PC under Windows or on a Macintosh
under OS X.

Program must be robust \-- Assume most users are not
computer-knowledgeable. System should be able to recover from errors,
particularly input mistakes.


## Theater Ticket Terminology

Each ***seat*** is occupied by a ***ticket-holder*.** A ticket may be
either purchased by the holder or received as a complimentary unpaid
ticket (a \"comp\" ticket). A ***show*** or ***model.performance.production*** is an
entire run of one or more ***performances*** of the same theatrical
work.

A ***season*** is a series of productions, usually covering a period
from August to June of the following year. A ***season ticket*** is a
purchase made for attending one model.performance of each show in the
season, often at a discount from the sum of the individual tickets. A
season ticket will be for the same night of the week and the same seat
for each model.performance.production. Most organizations derive a major portion of
their revenue from season tickets.

A ***section*** is a seating area of a theater venue, usually
comprising a group of seats. A ***loge*** is a section in the rear of
a theater. A ***balcony*** is a seating area usually above and behind
most other seats. Any section may be closed off or otherwise marked as
unavailable for a particular show and/or individual model.performance.
