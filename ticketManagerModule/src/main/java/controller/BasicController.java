package controller;

import model.*;
import model.dataHelpers.SaveResult;
import model.ticketHolders.SeasonTicketHolder;
import model.ticketHolders.TicketHolder;

import javax.xml.bind.JAXBException;
import java.util.UUID;

public class BasicController implements controller {

    private Root root;


    /**
     * The standard constructor for BasicController. It creates a root and loads data from memory.
     */
    public BasicController(){
        root = new Root();
        root.loadData();
    }


    /** An overload constructor for test purposes only, it does not load data from memory, but rather lets the data be added in the constructor.
     * @param root
     */
    public BasicController(Root root) {
        this.root = root;
    }


    private Object createObject(UUID parentUUID, ObjectType objectType, Object data){
        //Holds the object that is reutnred.
        Object parent;
        if (objectType != ObjectType.SEASON && objectType != ObjectType.S_TICKET_HOLDER) {
            //Get the parent by its uuid
            parent = this.deepSearchModle(parentUUID);
        } else {
            parent = root;
        }

        //If parent did not exist, go ahead and return null
        if (parent == null) {
            return null;
        }

        //If parent did exist, do a switch statment to make sure its the right type and then add its child.
        try {
            switch (objectType) {
                case SEASON -> {
                    if(parent instanceof Root){
                        return createSeason((Season) data);
                    }
                    return null;
                }
                case PRODUCTION -> {
                    if(parent instanceof Season){
                        return createNewProduction((Season) parent, (Production) data);
                    }
                    return null;
                }
                case PERFORMANCE -> {
                    if(parent instanceof Production){
                        return createNewPerformance((Production) parent, (Performance)data);
                    }
                    return null;
                }
                case TICKET_HOLDER -> {
                    if(parent instanceof Seat){
                        return createNewTicketholder((Seat)parent, (TicketHolder)data);
                    }
                    return null;
                }
                case S_TICKET_HOLDER -> {
                    if(parent instanceof Root){
                        return createSeasonTicketHolder((SeasonTicketHolder) data);
                    }
                    return null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    @Override
    public Object create_object(UUID parentUUID, ObjectType objectType, Object data) {
        //Call function that actually does the thing
        Object result = this.createObject(parentUUID, objectType, data);

        //If the result actually did anything, save to main memory.
        if(result != null){
            SaveResult saveResult =root.saveData();
            if(saveResult != SaveResult.SUCCEEDED){
                System.out.println(saveResult);
            }
        }

        //Return the result either way.
        return result;
    }

    @Override
    public Object get_object_ref(UUID uuid) {
       return deepSearchModle(uuid);
    }


    @Override
    public Object get_object_data(UUID uuid) {

        //Handles JAXB exceptions for deep cloning
        try {
            //Use deepSearchModle method to find the object
            SavableObject searchResult = this.deepSearchModle(uuid);

            //If the serach results are null, return null rather then continuing on
            if(searchResult == null){
                return null;
            }
            //Create a clone of the found object so that editing it in the view does not change the model.
            return  CloneHelper.deepClone(searchResult);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public SaveResult save_to_memory() {
        return this.root.saveData();
    }

    @Override
    public Root get_root_object() {
        return this.root;
    }


    /**
     * Search for a savable object by uuid and get its reference.
     * @param uuid The uuid of the object you want to find
     * @return A reference to that object if it exist, null otherwise. Needs to be cast
     * before use.
     */
    private SavableObject deepSearchModle(UUID uuid) {

        for (SeasonTicketHolder seasonTicketHolder : root.getSeasonTicketHolders()) {
            if (seasonTicketHolder.getUuid() == uuid) {
                return seasonTicketHolder;
            }
        }

        //Search through season
        for (Season s : root.getSeasonsList()) {
            //If season does match uuid, then return it.
            if (s.getUuid() == uuid) {
                return s;
            }

            //If season did not have it, check all of the productions
            for (Production p : s.getProductionsList()) {
                if (p.getUuid() == uuid) {
                    return p;
                }

                //If the production did not match, check its children.
                for (Performance pf : p.getPerformances()) {
                    if (pf.getUuid() == uuid) {
                        return pf;
                    }

                    //Since their is only one facility per performance, this one does not need a for loop.
                    if (pf.getFacility().getUuid() == uuid) {
                        return pf.getFacility();
                    }

                    for (Section sec : pf.getFacility().getSections()) {
                        if (sec.getUuid() == uuid) {
                            return sec;
                        }

                        for (Row r : sec.getRows()) {
                            if (r.getUuid() == uuid) {
                                return r;
                            }

                            for (Seat seat : r.getSeats()) {
                                if (seat.getUuid() == uuid) {
                                    return seat;
                                }

                                if (seat.getOccupant() != null) {
                                    if (seat.getOccupant().getUuid() == uuid) {
                                        return seat.getOccupant();
                                    }
                                }
                            }//Seat for loop
                        }//Row for loop
                    }//Section For loop, Facility doesn't need one cause their is only one facility per section.
                } //Performance for loop
            } //Productions for loop
        } //Seasons for loop
        return null;
    }



    /**
     * Helper funtion that adds a season to root as a copy.
     *
     * @param data
     * @return
     * @throws JAXBException
     */
    private Season createSeason(Season data) throws JAXBException {
        Season newSeason = (Season) CloneHelper.cloneExceptUUID((Season) data);

        root.addExistingSeason(newSeason);

        return newSeason;

    }

    private SeasonTicketHolder createSeasonTicketHolder(SeasonTicketHolder data) throws JAXBException {
        SeasonTicketHolder newSeasonTicketHolder = (SeasonTicketHolder) CloneHelper.cloneExceptUUID((SeasonTicketHolder) data);

        root.addExistingSeasonTicketHolder(newSeasonTicketHolder);

        return newSeasonTicketHolder;
    }

    private Production createNewProduction(Season parent, Production data) throws JAXBException {
        Production newProduction = (Production) CloneHelper.cloneExceptUUID(data);

        parent.addExistingProduction(newProduction);

        return newProduction;
    }

    private Performance createNewPerformance(Production parent, Performance data) throws JAXBException {
        Performance newPerformance = (Performance) CloneHelper.cloneExceptUUID(data);

        parent.addExistingPerformance(newPerformance);

        return newPerformance;
    }

    private TicketHolder createNewTicketholder(Seat parent, TicketHolder data) throws JAXBException {
        TicketHolder newTicketHolder = (TicketHolder) CloneHelper.cloneExceptUUID(data);
        parent.setOccupant(newTicketHolder, false);

        return newTicketHolder;
    }
}