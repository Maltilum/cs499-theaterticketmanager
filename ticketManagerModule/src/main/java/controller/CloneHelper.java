package controller;


import model.SavableObject;
import view.xmlUtils.XmlDataHolder;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBException;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Helps with creating deep copies of object, aka ones that don't cause changes in the orgional.
 */
public class CloneHelper {


    /** Creates a deep clone of the given object, meaning that the data is all the same
     * But chaning one will not change the other.
     * @param obj
     * @return
     * @throws JAXBException
     */
    public  static Object deepClone(SavableObject obj) throws JAXBException {
        StringWriter sw = new StringWriter();
        obj.getJAXBContext().createMarshaller().marshal(obj, sw);

        String xmlString = sw.toString();

        StringReader stringReader = new StringReader(xmlString);

        return obj.getJAXBContext().createUnmarshaller().unmarshal(stringReader);

    }


    /** Clone an object, but give it a new uuid.
     * @param obj
     * @return A reference to the newly cloned object.
     * @throws JAXBException
     */
    public static Object cloneExceptUUID(SavableObject obj) throws JAXBException {
        StringWriter sw = new StringWriter();
        obj.getJAXBContext().createMarshaller().marshal(obj, sw);

        String xmlString = sw.toString();

        StringReader stringReader = new StringReader(xmlString);

        SavableObject newObj = (SavableObject) obj.getJAXBContext().createUnmarshaller().unmarshal(stringReader);
        newObj.regenerateUUID();

        return newObj;
    }
}
