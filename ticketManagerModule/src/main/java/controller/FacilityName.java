package controller;

public enum FacilityName {
    EMPTY(null),
    PLAYHOUSE("Civic Center Playhouse"),
    CONCERT_HALL("Civic Center Concert Hall");

    public final String label;

    private FacilityName(String label) {
        this.label = label;
    }

    public FacilityName valueOfLabel(String label) {
        for(FacilityName facilityName : values()) {
            if (facilityName.label.equals(label)) {
                return facilityName;
            }
        }
        return null;
    }


    @Override
    public String toString() {
        return this.label;
    }
}
