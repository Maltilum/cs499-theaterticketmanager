package controller;

public enum ObjectType {
    SEASON,
    PRODUCTION,
    PERFORMANCE,
    FACILITY,
    SECTION,
    ROW,
    SEAT,
    TICKET_HOLDER,
    S_TICKET_HOLDER,
    ROOT,
}
