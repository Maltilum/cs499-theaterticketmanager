package controller;

import model.Root;
import model.dataHelpers.SaveResult;

import java.util.UUID;

public interface controller {


    /** This method creates a new object and adds it to the model. You need to give the uuid of the new object's parent, aka what it will be under.
     * You also need to create a new object of the same type and feed it in as object data
     * This is object is not added, but instead shallow cloned and then added.
     * The cloning does not include its UUID or any lists under it.
     * This prevents objects from being put back in, and ending up with copies.
     * @param parentUUID The uuid of the parent object
     * @param objectType The type of object you want to create, represetned by an ObjectType.
     * @param data an instance of the type of object you want to create with the data you want it to have. UUID is not carried over.
     * @return refernce to the created object.
     */
    public Object create_object(UUID parentUUID, ObjectType objectType, Object data);



    /**
     * @param uuid The uuid of the object you want to get a reference to.
     * @return The object reference to the object you wanted, or null if the UUID did not exit.
     * Edditng this well edit the origional
     */
    public Object get_object_ref(UUID uuid);


    /** Gets the object's data, but in a new instance of the object. This is useful if you want a safe copy of an object on which edits will not reflect in their modle.
     * @param uuid The uuid of the object
     * @return A deep clone of the object itself, meaning changes in it will not be reflect in the orgional
     */
    public Object get_object_data(UUID uuid);


    /** Save the current modle to hard memory.
     * @return The result of the save, as in weather it suceed or failed.
     */
    public SaveResult save_to_memory();

    /**
     * @return The root save object
     */
    public Root get_root_object();
}
