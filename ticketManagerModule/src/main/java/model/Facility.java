/* Author: Nicholas Burns
 * Created on: 8/30/2020
 */
package model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.UUID;

/**
 * The class to represent a Facility
 */
@XmlRootElement(name = "Facility")
@XmlAccessorType(XmlAccessType.FIELD)
public class Facility extends SavableObject {
    private int capacity;

    @XmlElement(name = "Section")
    private ArrayList<Section> sections;

    /**
     * Constructor for creating a Facility object
     *
     * @param facilityName The name of the facility
     * @param capacity     The maximum capacity of the facility
     * @param sections     The list of Sections in the facility
     */
    public Facility(String facilityName, int capacity, ArrayList<Section> sections) {
        super();
        this.setDisplayName(facilityName);
        this.capacity = capacity;
        this.sections = sections;
    }

    /**
     * Constructor that sets the default values for the facility;
     */
    public Facility() {
        super();
        this.capacity = 0;
        this.sections = new ArrayList<Section>();

    }

    public Facility(String name) {
        super();
        this.capacity = 0;
        this.sections = new ArrayList<Section>();
        this.setDisplayName(name);
    }

    /**
     * Gets the capacity of the facility
     *
     * @return The maximum capacity of the facility
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Sets the maximum capacity of the facility
     *
     * @param capacity The new capacity of the facility
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    /**
     * Gets the ArrayList of sections in the facility
     *
     * @return The ArrayList of Sections in the Facility
     */
    @XmlTransient
    public ArrayList<Section> getSections() {
        return sections;
    }

    /**
     * Gets a single Section from the facility
     *
     * @param uuid the {@link UUID} of the section
     * @return The section with a matching {@link UUID} or null if no section matches the uuid
     */
    public Section getSection(UUID uuid) {
        for (Section section : sections) {
            if (section.getUuid().equals(uuid)) {
                return section;
            }
        }

        return null;
    }

    /**
     * Sets the ArrayList of Sections in the Facility
     *
     * @param sections The new ArrayList of Sections
     */
    public void setSections(ArrayList<Section> sections) {
        this.sections = sections;
    }

    /**
     * Adds a new Section to the Facility
     *
     * @param section The new Section to be added
     */
    public boolean addExistingSection(Section section) {
        for (int i = 0; i < sections.size(); i++) {
            if (sections.get(i).getUuid().compareTo(section.getUuid()) == 0) {
                return false;
            }
        }

        return this.sections.add(section);
    }

    /**
     * Create new section and add it to sections list.
     *
     * @param name name of new section
     * @return Section added if it was added, null if itwas not added.
     */
    public Section addNewSection(String name) {
        Section s = new Section(name);

        if (this.addExistingSection(s)) {
            return s;
        }

        return null;

    }

    /**
     * Removes a Section that has a matching {@link UUID}
     *
     * @param uuid The {@link UUID} of the Section to be removed
     * @return True if a section is removed; false if there is no matching Section
     */
    public boolean removeSection(UUID uuid) {
        return sections.removeIf(section -> (section.getUuid().equals(uuid)));
    }

    /**
     * Searches through the list for the Section with a matching UUID
     *
     * @param uuid The UUID of the section to be returned
     * @return The Section with a matching UUID
     */
    public Section searchSection(UUID uuid) {
        return (Section) searchSavableListUUID(uuid, sections);
    }

    @Override
    public boolean equals(Object obj) {
        if (!obj.getClass().equals(Facility.class)) {
            return false;
        }

        Facility facility = (Facility) obj;
        return this.getUuid().equals(facility.getUuid()) &&
                this.capacity == facility.getCapacity() &&
                this.sections.equals(facility.getSections());
    }
}
