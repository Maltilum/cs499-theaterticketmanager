/* Author: Nicholas Burns
 * Created on: 9/14/2020
 */
package model;

import controller.FacilityName;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Class to represent a model.performance of a model.performance.production
 */
@XmlRootElement(name = "Performance")
@XmlAccessorType(XmlAccessType.FIELD)
public class Performance extends SavableObject {
    private Date dateAndTime;
    private boolean isCancelled;
    private Facility facility;

    private FacilityName facilityName;

    /**
     * Constructor for a Performance object to represent a single showing of some model.performance.production
     */
    public Performance() {
        super();
        this.dateAndTime = new Date();
        this.isCancelled = false;
        this.facility = new Facility();
        this.facilityName = FacilityName.EMPTY;
    }

    /**
     * Constructor for a Performance object to represent a single showing of some model.performance.production
     *
     * @param title       The title of the model.performance
     * @param dateAndTime The date and time at which the model.performance will occur
     * @param isCancelled Whether the model.performance has been cancelled
     * @param facility    The facility at which the model.performance will take place
     */
    public Performance(String title, Date dateAndTime, boolean isCancelled, Facility facility) {
        this.setDisplayName(title);
        this.dateAndTime = dateAndTime;
        this.isCancelled = isCancelled;
        this.facility = facility;
    }

    public Performance(String title) {
        this.isCancelled = false;
        this.setDisplayName(title);
    }

    /**
     * Getter method for the {@link Date} of the model.performance
     *
     * @return The date of the model.performance
     */
    public Date getDateAndTime() {
        return dateAndTime;
    }

    /**
     * Sets the {@link Date} for when the model.performance will occur
     *
     * @param dateAndTime The new Date of the model.performance
     */
    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    /**
     * Getter method for whether the model.performance has been cancelled
     *
     * @return True if the model.performance has been cancelled, false otherwise
     */
    public boolean isCancelled() {
        return isCancelled;
    }

    /**
     * Sets whether the model.performance will occur
     *
     * @param cancelled True if the model.performance has been cancelled, false otherwise
     */
    public void setCancelled(boolean cancelled) {
        isCancelled = cancelled;
    }

    /**
     * Gets the {@link Facility} at which the model.performance will take place
     *
     * @return The facility where the model.performance will occur
     */
    public Facility getFacility() {
        return facility;
    }

    /**
     * Sets the {@link Facility} at which the model.performance will take place
     *
     * @param facility The new facility for the model.performance
     */
    public void setFacility(Facility facility) {
        this.facility = facility;
    }

    /**
     * Gets the name of the facility at which the model.performance will take place
     *
     * @return The facility where the model.performance will occur
     */
    public FacilityName getFacilityName() {
        return facilityName;
    }

    /**
     * Sets the name of the facility at which the model.performance will take place
     *
     * @param facilityName The new facility for the model.performance
     */
    public void setFacilityName(FacilityName facilityName) {
        this.facilityName = facilityName;
    }

    @Override
    public String toString() {
        return dateAndTime.toString();
    }
}
