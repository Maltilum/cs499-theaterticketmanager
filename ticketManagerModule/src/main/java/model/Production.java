package model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.UUID;

/**
 * This class holds the needed functions for the model.performance.production object
 * A model.performance.production is a group of shows taking place with-in a season
 * parent object is season while child object is model.performance
 */
@XmlRootElement(name = "Production")
public class Production extends SavableObject {


    ArrayList<Performance> performances;

    /**
     * The default constructor for the Production class
     */
    public Production() {
        super();
        this.setDisplayName("Untitled Production");
        this.performances = new ArrayList<>();
    }

    /**
     * Constructor for the Production class
     *
     * @param productionName The name of the model.performance.production
     */
    public Production(String productionName) {
        super();
        this.performances = new ArrayList<Performance>();
        this.setDisplayName(productionName);
    }


    /**
     * Sets the list of performances in the Production
     *
     * @param performances The new list of performances
     */
    public void setPerformances(ArrayList<Performance> performances) {
        this.performances = performances;
    }

    /**
     * Adds a model.performance to the list of performances
     *
     * @param performance The model.performance to be added
     * @return True if the model.performance was successfully added
     */
    public boolean addExistingPerformance(Performance performance) {
        if (performance == null) {
            return false;
        }

        for (int i = 0; i < this.performances.size(); i++) {
            if (this.performances.get(i).getUuid().compareTo(performance.getUuid()) == 0) {
                return false;
            }
        }
        return this.performances.add(performance);
    }

    /**
     * Create a new performance and try to add it to the list of performances.
     *
     * @param performanceName The name of the new perfomrance
     * @return The perfomrance, or null if adding it failed.
     */
    public Performance addNewPerformance(String performanceName) {
        Performance p = new Performance(performanceName);
        if (this.addExistingPerformance(p)) {
            return p;
        }
        return null;
    }

    /**
     * Removes a model.performance if a model.performance with a matching UUID exists
     *
     * @param uuid The UUID of the model.performance to be removed
     * @return True if the model.performance was removed
     */
    public boolean removePerformance(UUID uuid) {
        return this.performances.removeIf(performance -> (performance.getUuid().equals(uuid)));
    }

    /**
     * Getter method for the list of Performances
     *
     * @return The list of Performances in the Production
     */
    @XmlElement(name = "Performance")
    public ArrayList<Performance> getPerformances() {
        return performances;
    }

    /**
     * Gets a model.performance with the specified UUID
     *
     * @param uuid The UUID of the model.performance to be retrieved
     * @return The Performance if a matching Performance exists or null
     */
    public Performance getPerformance(UUID uuid) {
        for (Performance performance : this.performances) {
            if (performance.getUuid().compareTo(uuid) == 0) {
                return performance;
            }
        }
        return null;
    }


    /**
     * Sorts the performances by UUID
     */
    public void sortPerformances() {
        this.performances.sort(((o1, o2) -> o1.getUuid().compareTo(o2.getUuid())));
    }

    /**
     * Searches the list of Performances for one with a matching UUID
     *
     * @param uuid The UUID of the performance to be found
     * @return The Performance with a matching UUID
     */
    public Performance searchPerformance(UUID uuid) {
        return (Performance) searchSavableListUUID(uuid, performances);
    }

    @Override
    public String toString() {
        return this.getDisplayName();
    }
}

