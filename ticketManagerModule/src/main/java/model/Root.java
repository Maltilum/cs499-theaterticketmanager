package model;

import model.dataHelpers.BasicSaveStructure;
import model.dataHelpers.DataAccessManager;
import model.dataHelpers.SaveResult;
import model.ticketHolders.SeasonTicketHolder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Serves as the Root of the model portion of the program.
 */
@XmlRootElement(name = "data")
public class Root {


    //A list of seasons
    @XmlElement(name = "Season")
    private List<Season> seasonsList;

    //Season Ticket Holders
    @XmlElement
    private List<SeasonTicketHolder> seasonTicketHolders;

    //Data helper
    private DataAccessManager dataAccessManager;

    public Root() {
        this.dataAccessManager = new DataAccessManager();
        this.seasonsList = new ArrayList<>();
        this.seasonTicketHolders = new ArrayList<>();
    }

    public void loadData() {
        BasicSaveStructure data = this.dataAccessManager.loadBasic("mainData.xml");
        if(data == null){
            return;
        }
        if(data.getSeasonsList() != null) {
            this.seasonsList = data.getSeasonsList();
        }
        if (data.getSeasonTicketHoldersList() != null) {
            this.seasonTicketHolders = data.getSeasonTicketHoldersList();
        }
    }

    public Facility loadConcertHall() {
        Facility data = this.dataAccessManager.loadFacility("facilities/ConcertHall.xml");
        if(data == null){
            return null;
        }
        return data;
    }

    public Facility loadPlayHouse() {
        Facility data = this.dataAccessManager.loadFacility("facilities/Playhouse.xml");
        if(data == null){
            return null;
        }
        return data;
    }

    public SaveResult saveData() {
        BasicSaveStructure data = new BasicSaveStructure();
        data.setSeasonsList(this.seasonsList);
        data.setSeasonTicketHoldersList(this.seasonTicketHolders);
        return this.dataAccessManager.saveObject(data, "mainData.xml");
    }

    /**
     * Creates a new season with the minimum viable information and then adds it to the list of seasons automatically.
     *
     * @param name      Name of the season.
     * @param startDate When the season starts.
     * @param endDate   When the season ends.
     * @return If it worked, returns the new season's uuid. If it failed, returns null.
     */
    public Season createNewSeason(String name, Date startDate, Date endDate) {
        Season season = new Season(name, startDate, endDate);
        if (this.addExistingSeason(season)) {
            return season;
        }
        return null;
    }


    /**
     * Adds a season to the list of seasons.
     *
     * @param s Season to add.
     * @return True if was added, False if was not. If it wasn't added; it is because the s is null or already in the list.
     */
    public boolean addExistingSeason(Season s) {
        if (s == null) {
            return false;
        }
        else if(seasonsList == null) {
            this.seasonsList = new ArrayList<>();
        }
        else if (seasonsList.contains(s)) {
            return false;
        }
        this.seasonsList.add(s);
        return true;

    }

    /**
     * Adds a SeasonTicketHolder to the list of SeasonTicketHolders.
     *
     * @param sth Season to add.
     * @return True if was added, False if was not. If it wasn't added; it is because the sth is null or already in the list.
     */
    public boolean addExistingSeasonTicketHolder(SeasonTicketHolder sth) {
        if (sth == null) {
            return false;
        }
        else if (seasonTicketHolders == null) {
            this.seasonTicketHolders = new ArrayList<>();
        }
        else if (seasonTicketHolders.contains(sth)) {
            return false;
        }
        this.seasonTicketHolders.add(sth);
        return true;
    }


    //----------------------------------------------------------------------------- Getters and Setters----------------

    /**
     * @return the list of seasons.
     */
    public List<Season> getSeasonsList() {
        return seasonsList;
    }

    public List<SeasonTicketHolder> getSeasonTicketHolders() {
        return seasonTicketHolders;
    }

    /**
     * Search Root's list of seasons for one with a matching UUID
     *
     * @param uuid The UUID of the Season to be found
     * @return The Season with a matching UUID
     */
    public Season searchSeasons(UUID uuid) {
        return (Season) SavableObject.searchSavableListUUID(uuid, seasonsList);
    }

}
