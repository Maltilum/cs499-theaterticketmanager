package model;/* Author: Nicholas Burns
 * Created on: 8/30/2020
 */

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.UUID;

@XmlRootElement(name = "Row")
@XmlAccessorType(XmlAccessType.FIELD)
public class Row extends SavableObject {
    private float price;

    @XmlElement(name = "Seat")
    private ArrayList<Seat> seats;


    /**
     * Constructor for a Row
     *
     * @param rowName The name of the Row
     * @param price   The price of the Row
     * @param seats   The ArrayList of {@link Seat} in the Row
     */
    public Row(String rowName, float price, ArrayList<Seat> seats) {
        super();
        this.price = price;
        this.seats = seats;
        this.setDisplayName(rowName);

    }

    public Row(String rowName) {
        super();
        this.setDisplayName(rowName);
        this.price = 0.00f;
        this.seats = new ArrayList<Seat>();
    }

    /**
     * Default constructor for a Row
     */
    public Row() {
        super();
        this.seats = new ArrayList<Seat>();

    }


    /**
     * Getter method for the price of the Row
     *
     * @return The price of the Row
     */
    public float getPrice() {
        return this.price;
    }

    /**
     * Sets the price of the Row
     *
     * @param price The float for the new price of the Row
     */
    public void setPrice(float price) {
        this.price = price;
        for (Seat seat : seats) {
            seat.setPrice(price);
        }
    }

    /**
     * Gets the ArrayList of {@link Seat} in the Row
     *
     * @return The Arraylist of {@link Seat}
     */
    @XmlTransient
    public ArrayList<Seat> getSeats() {
        return seats;
    }

    /**
     * Gets the {@link Seat} with a matching {@link UUID}
     *
     * @param uuid The {@link UUID} of the {@link Seat} to be found
     * @return A {@link Seat} with a matching {@link UUID} or null if no match is found
     */
    public Seat getSeat(UUID uuid) {
        for (Seat seat : seats) {
            if (seat.getUuid().equals(uuid)) {
                return seat;
            }
        }

        return null;
    }

    /**
     * Sets the ArrayList of {@link Seat} in the Row
     *
     * @param seats The new ArrayList of {@link Seat}
     */
    public void setSeats(ArrayList<Seat> seats) {
        this.seats = seats;
    }

    /**
     * Adds a {@link Seat} to the ArrayList of Seats
     *
     * @param seat The Seat to be added
     */
    public boolean addExistingSeat(Seat seat) {
        if (seat == null) {
            return false;
        }

        for (int i = 0; i < this.seats.size(); i++) {
            if (this.seats.get(i).getUuid().compareTo(seat.getUuid()) == 0) {
                return false;
            }
        }

        return this.seats.add(seat);
    }

    public Seat addNewSeat(String name) {
        Seat s = new Seat(name);
        if (addExistingSeat(s)) {
            return s;
        }
        return null;
    }

    /**
     * Removes a {@link Seat} that has a matching {@link UUID}
     *
     * @param uuid The {@link UUID} of the {@link Seat} to be removed
     * @return True if a {@link Seat} is removed; false if there is no matching {@link Seat}
     */
    public boolean removeSeat(UUID uuid) {
        return seats.removeIf(seat -> (seat.getUuid().equals(uuid)));
    }


    @Override
    public String toString() {
        return this.getDisplayName();
    }
}
