package model;


import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Abstract class that simplifies saving objects to persistent memory quite a lot.
 * Any object that needs to saved to persistent memory should inherit this class.
 */
@XmlRootElement
public abstract class SavableObject implements Comparable<SavableObject> {

    private String className = this.getClass().getSimpleName();
    //Provides a common name for human readable id.
    private String displayName = this.getClass().getCanonicalName();


    //These provide a unique identifier for the data of an object.
    private UUID uuid;

    /**
     * Empty declaration. Needed for proper saving.
     */
    public SavableObject() {
        //Initialize uuid and create a string id.
        uuid = UUID.randomUUID();
    }


    /**
     * @return The context needed to marshal and unmarshal the class into xml.
     * @throws JAXBException if the class cannot be marshaled or the context is invalid, it'll cause problems.
     */
    public JAXBContext getJAXBContext() throws JAXBException {
        return JAXBContext.newInstance(this.getClass());
    }

    /**
     * @return returns the common name of the SavableObject, usually in the form of its Canonical class name
     */
    public String getDisplayName() {
        return displayName;
    }


    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @XmlAttribute
    public UUID getUuid() {
        return uuid;
    }


    @Override
    public int compareTo(SavableObject obj) {
        return this.getUuid().compareTo(obj.getUuid());
    }


    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public UUID regenerateUUID(){
        this.uuid = UUID.randomUUID();
        return this.uuid;
    }

    /**
     * @param uuid The uuid of the object you want to search for.
     * @param list The list you wish to search
     * @return An array list of SavableObjects that matched. You will need to cast them into whatever object type they actually are to use them in that context.
     */
    public static List<SavableObject> searchSavableListUUID(UUID uuid, List<? extends SavableObject> list) {

        List<SavableObject> outList = new ArrayList<>();

        for (SavableObject obj : list) {
            if (obj.getUuid().compareTo(uuid) == 0) {
                outList.add(obj);
            }
        }
        return outList;
    }

}
