/* Author: John Trudell
 * Created on: 9/19/2020
 */


package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * Class to representa model.performance of a season
 */
@XmlRootElement(name = "Season")
@XmlAccessorType(XmlAccessType.FIELD)
public class Season extends SavableObject {


    @XmlElement
    private Date startDate;
    @XmlElement
    private Date endDate;
    @XmlElement(name = "Production")
    private List<Production> productionsList;


    /**
     * Constructor for a Season object to represent a single showing of some season
     */
    public Season() {
        super();
        this.productionsList = new ArrayList<Production>();
        this.startDate = null;
        this.endDate = null;
    }

    public Season(String name, Date startDate, Date endDate) {
        super();
        this.productionsList = new ArrayList<Production>();
        this.startDate = startDate;
        this.endDate = endDate;
        this.setDisplayName(name);
    }


    /**
     * Create a new production with a certain name, try to add it to productions, and return it.
     *
     * @param name The name of the new production.
     * @return if it was added right, the production. If something went wrong null.
     */
    public Production addNewProduction(String name) {
        Production p = new Production(name);
        if (this.addExistingProduction(p)) {
            return p;
        }
        return null;
    }

    /**
     * Adds an existing production to the list. If that produciton is already in the list, then it does not do that.
     *
     * @param production The production you wish to add
     * @return True if it was added, false if it wasn't because it was already in the list.
     */
    public boolean addExistingProduction(Production production) {

        if (production == null) {
            return false;
        }
        for (int i = 0; i > productionsList.size(); i++) {
            if (productionsList.get(i).getUuid().compareTo(production.getUuid()) == 0) {
                return false;
            }
        }
        productionsList.add(production);
        return true;

    }

    /**
     * @return The entire list of productions.
     */
    public List<Production> getProductionsList() {
        return productionsList;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Searches the list of Productions for the Production with the specified UUID
     *
     * @param uuid The UUID of the Production to be returned
     * @return The Production with a matching UUID
     */
    public Production searchProductions(UUID uuid) {
        return (Production) searchSavableListUUID(uuid, productionsList);
    }

    @Override
    public String toString() {
        return this.getDisplayName();
    }
}


