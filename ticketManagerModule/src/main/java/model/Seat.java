package model;

import model.ticketHolders.TicketHolder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Seat extends SavableObject {

    //The price of the seat
    private float price;

    @XmlElement(name = "TicketHolder")
    //The ticket holder that has reserved or bought the seat
    private TicketHolder occupant;

    private boolean isAccessible = false;


    /**
     * Basic constructor with all fields
     *
     * @param price    The price of the seat
     * @param t_holder the ticket holder that has reserved the seat
     */
    public Seat(float price, TicketHolder t_holder) {
        super();
        this.price = price;
        this.occupant = t_holder;

    }

    public Seat(String displayName) {
        super();
        this.price = 0.00f;
        this.setDisplayName(displayName);
    }


    public Seat() {
        super();
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }


    /**
     * @param t              Ticket holder to to make the seat's occupant
     * @param removeExisting If True, will override current occupant if one exist. If false, if their is an occupant; refuse to replace them.
     * @return If the set worked or not.
     */
    public boolean setOccupant(TicketHolder t, Boolean removeExisting) {
        if (t == null) {
            return false;
        }
        if (!removeExisting && this.occupant != null) {
            return false;
        }

        this.occupant = t;
        return true;


    }

    public TicketHolder createOccupant(String name, Boolean removeExisting) {

        TicketHolder t = new TicketHolder(name);
        if (this.setOccupant(t, removeExisting)) {
            return t;
        }
        return null;

    }

    public TicketHolder getOccupant() {
        return this.occupant;
    }

    /**
     * Gets whether the seat is accessible to people with disabilities
     *
     * @return True if it is accessible
     */
    public boolean isAccessible() {
        return isAccessible;
    }

    /**
     * Sets whether the seat is accessible to people with disabilities
     *
     * @param accessible Boolean representing accessibility
     */
    public void setAccessible(boolean accessible) {
        isAccessible = accessible;
    }

    @Override
    public String toString() {
        return getDisplayName();
    }
}
