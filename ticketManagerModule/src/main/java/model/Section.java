package model;/* Author: Nicholas Burns
 * Created on: 8/30/2020
 */

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Represents a Section in a {@link Facility}
 */
@XmlRootElement(name = "Section")
@XmlAccessorType(XmlAccessType.FIELD)
public class Section extends SavableObject {
    private boolean sectionAvailable;
    private float sectionPrice;

    @XmlElement(name = "Row")
    private ArrayList<Row> rows;

    /**
     * Constructor for a Section object
     *
     * @param sectionAvailable The availability of the Section
     * @param sectionPrice     The price of seats in the Section
     * @param sectionName      The name of the Section
     * @param rows             The ArrayList of {@link Row} in the section
     */
    public Section(boolean sectionAvailable, float sectionPrice, String sectionName, ArrayList<Row> rows) {
        super();
        this.sectionAvailable = sectionAvailable;
        this.sectionPrice = sectionPrice;
        this.setDisplayName(sectionName);
        this.rows = rows;

    }

    /**
     * Default constructor for a Section object
     */
    public Section() {
        super();
        this.sectionAvailable = false;
        this.sectionPrice = 0;
        this.setDisplayName("Untitled Section");
        this.rows = new ArrayList<>();
    }

    public Section(String name) {
        super();
        this.sectionAvailable = false;
        this.sectionPrice = 0;
        this.setDisplayName(name);
        this.rows = new ArrayList<>();
    }


    /**
     * Method to check the availability of the section
     *
     * @return True if the section is available; false otherwise
     */
    public boolean isSectionAvailable() {
        return sectionAvailable;
    }

    /**
     * Sets the availability of the Section
     *
     * @param sectionAvailable The new availability of the section
     */
    public void setSectionAvailable(boolean sectionAvailable) {
        this.sectionAvailable = sectionAvailable;
    }

    /**
     * Gets the price of seats in the section
     *
     * @return The price of seats in the section
     */
    public float getSectionPrice() {
        return sectionPrice;
    }

    /**
     * Sets the price of seats in the section
     *
     * @param sectionPrice the new price of seats in the section
     */
    public void setSectionPrice(float sectionPrice) {
        this.sectionPrice = sectionPrice;
        for(Row row : rows) {
            row.setPrice(sectionPrice);
        }
    }

    /**
     * Returns the ArrayList of the {@link Row} in the section
     *
     * @return The ArrayList of {@link Row} in the section
     */
    @XmlTransient
    public ArrayList<Row> getRows() {
        return rows;
    }

    /**
     * Gets the {@link Row} with a matching {@link UUID}
     *
     * @param uuid The {@link UUID} of the {@link Row} to be found
     * @return A {@link Row} with a matching {@link UUID} or null if no match is found
     */
    public Row getRow(UUID uuid) {
        for (Row row : rows) {
            if (row.getUuid().equals(uuid)) {
                return row;
            }
        }

        return null;
    }

    /**
     * Sets the ArrayList of {@link Row} in the section
     *
     * @param rows The new ArrayList of {@link Row}
     */
    public void setRows(ArrayList<Row> rows) {
        this.rows = rows;
    }

    /**
     * Adds a {@link Row} to the ArrayList of Rows
     *
     * @param row The Row to be added
     */
    public boolean addExistingRow(Row row) {

        if (row == null) {
            return false;
        }

        for (int i = 0; i < this.rows.size(); i++) {
            if (this.rows.get(i).getUuid().compareTo(row.getUuid()) == 0) {
                return false;
            }
        }

        return this.rows.add(row);
    }


    /**
     * Creates and adds a new row.
     *
     * @param name name of row
     * @return The row, or Null if failed.
     */
    public Row addNewRow(String name) {
        Row r = new Row(name);
        if (this.addExistingRow(r)) {
            return r;
        }
        return null;
    }

    /**
     * Removes a {@link Row} that has a matching {@link UUID}
     *
     * @param uuid The {@link UUID} of the {@link Row} to be removed
     * @return True if a {@link Row} is removed; false if there is no matching {@link Row}
     */
    public boolean removeRow(UUID uuid) {
        return rows.removeIf(row -> (row.getUuid().equals(uuid)));
    }

    @Override
    public boolean equals(Object obj) {
        if (!obj.getClass().equals(Section.class)) {
            return false;
        }

        Section section = (Section) obj;
        return this.getUuid().equals(section.getUuid()) &&
                this.sectionAvailable == section.isSectionAvailable() &&
                this.sectionPrice == section.getSectionPrice() &&
                this.rows.equals(section.getRows());
    }

    @Override
    public String toString() {
        return this.getDisplayName();
    }
}
