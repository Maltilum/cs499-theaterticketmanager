package model.dataHelpers;

import model.SavableObject;
import model.Season;
import model.ticketHolders.SeasonTicketHolder;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "data")
public class BasicSaveStructure extends SavableObject {


    private List<Season> seasonsList;


    private List<SeasonTicketHolder> SeasonTicketHoldersList;

    public BasicSaveStructure(){
        super();
        setDisplayName("MainData");
    }

    @XmlElement(name = "Season")
    public List<Season> getSeasonsList() {
        return seasonsList;
    }


    public void setSeasonsList(List<Season> seasonsList) {
        this.seasonsList = seasonsList;
    }

    @XmlElement(name="SeasonTicketHolders")
    public List<SeasonTicketHolder> getSeasonTicketHoldersList() {
        return SeasonTicketHoldersList;
    }

    public void setSeasonTicketHoldersList(List<SeasonTicketHolder> seasonTicketHoldersList) {
        SeasonTicketHoldersList = seasonTicketHoldersList;
    }
}
