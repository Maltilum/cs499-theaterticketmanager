package model.dataHelpers;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvException;
import model.ticketHolders.PostalAddress;
import model.ticketHolders.SeasonTicketHolder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CSV_Factory {


    /**
     * Exports the name and address info of ax list of seasonTicketHolders to a file.
     *
     * @param seasonTicketHolderList A list of season ticket holders to export as a file.
     * @param path                   The path you want to export it to.
     * @throws IOException Possible file IO expcetions, say if files aren't readable etc.
     */
    public void exportAddressList(List<SeasonTicketHolder> seasonTicketHolderList, String path) throws IOException {
        //Create a list of string arrays that corresponds to a spreadsheet.
        List<String[]> lines = createAddressList(seasonTicketHolderList);
        CSVWriter writer = new CSVWriter(new FileWriter(path));

        for (String[] line : lines) {
            writer.writeNext(line);
        }

        writer.close();
    }


    /**
     * This method can take convert a CSV into a HashMap of names and addresses
     * when given a template for how the csv file is set up.
     *
     * @param columns An String array that needs to have 6 entries. Each entry
     *                needs to be the name of a variable from CsvAddress Record.
     *                Where the name of the variable is in the array of strings
     *                corresponds to where it should be in the Csv file. An example
     *                might be [name, street, line2, city, region, zipCode]. It doesn't
     *                matter what the names of the columns in the actually csv file are
     *                so long as the corresponding name of the variable is in the corresponding location.
     *                So the first element in the array corresponds to column 1, the second
     *                to column 2, and so on.
     * @param path    The file path to csv file you want to read.
     * @return An ArrayList of SeasonTicketHolders
     * @throws FileNotFoundException
     */
    public ArrayList<SeasonTicketHolder> importAddressListAdvanced(String[] columns, String path) throws FileNotFoundException {
        //Create a hash map to populate and then return.
        ArrayList<SeasonTicketHolder> seasonTicketHolders = new ArrayList<>();

        //Open the file
        CSVReader reader = new CSVReader(new FileReader(path));

        //Create mapping stratagy
        ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
        strategy.setType(CsvAddressRecord.class);
        strategy.setColumnMapping(columns);

        //Create csv bean reader
        CsvToBean csvToBean = new CsvToBeanBuilder(reader)
                .withMappingStrategy(strategy)
                .withSkipLines(1)
                .withIgnoreLeadingWhiteSpace(true)
                .build();

        //Iterate though each record, adding it to the hashmap
        for (CsvAddressRecord record : (Iterable<CsvAddressRecord>) csvToBean) {
            PostalAddress thisAddress = new PostalAddress(record.street, record.line2, record.city, record.region, record.zipCode);
            SeasonTicketHolder sth = new SeasonTicketHolder();
            sth.setDisplayName(record.name);
            sth.setAddress(thisAddress);
            seasonTicketHolders.add(sth);
        }

//        if(addresses.get("Name") != null){
//            addresses.remove("Name");
//        }

        return seasonTicketHolders;
    }


    /**
     * Imports a csv file that is of the same format that this class exports them.
     *
     * @param path Path to the csv file
     * @return an ArrayList of SeasonTicketHolders
     * @throws IOException
     * @throws CsvException
     */
    public ArrayList<SeasonTicketHolder> importAddressListBasic(String path) throws IOException, CsvException {
        String[] template = new String[6];
        template[0] = "name";
        template[1] = "street";
        template[2] = "line2";
        template[3] = "city";
        template[4] = "region";
        template[5] = "zipCode";

        return this.importAddressListAdvanced(template, path);

    }


    /**
     * Create a list of string arrays for use in creating CSV files.
     *
     * @param stList
     * @return A list of string arrays. Each entry in the list is an array that represents a line. Each element of the array is one field.
     */
    private List<String[]> createAddressList(List<SeasonTicketHolder> stList) {
        List<String[]> outList = new ArrayList<String[]>();

        String[] initEntry = new String[6];
        initEntry[0] = "Name";
        initEntry[1] = "Street";
        initEntry[2] = "Line 2";
        initEntry[3] = "City";
        initEntry[4] = "Region";
        initEntry[5] = "Zip Code";


        outList.add(initEntry);

        for (SeasonTicketHolder sth : stList) {
            String[] entry = new String[6];
            entry[0] = sth.getDisplayName();
            entry[1] = sth.getAddress().getStreet();
            entry[2] = sth.getAddress().getLine2();
            entry[3] = sth.getAddress().getCity();
            entry[4] = sth.getAddress().getRegion();
            entry[5] = sth.getAddress().getZipCode();
            outList.add(entry);
        }

        return outList;
    }

}
