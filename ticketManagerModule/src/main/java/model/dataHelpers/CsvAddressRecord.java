package model.dataHelpers;

import com.opencsv.bean.CsvBindByName;

public class CsvAddressRecord {

    //@CsvBindByName
    public String name;

    //@CsvBindByName
    public String street;

    //@CsvBindByName
    public String line2;

    //@CsvBindByName
    public String city;

    //@CsvBindByName
    public String region;

    //@CsvBindByName (column = "Zip Code")
    public String zipCode;

    public CsvAddressRecord(){

    }

    public CsvAddressRecord(String name, String street, String line2, String city, String region, String zipCode) {
        this.name = name;
        this.street = street;
        this.line2 = line2;
        this.city = city;
        this.region = region;
        this.zipCode = zipCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
