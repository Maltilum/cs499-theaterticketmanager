package model.dataHelpers;

import model.Facility;
import model.Root;
import model.SavableObject;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.io.*;

/**
 * Helper class that handles storing and retrieving object data from persistent memory.
 */
@SuppressWarnings({"ResultOfMethodCallIgnored", "UnusedReturnValue"})
public class DataAccessManager {

    private static final String DATA_PATH_ROOT = "./data/";
    private XMLFactory xmlFactory;

    public DataAccessManager() {
       xmlFactory = new XMLFactory();
        //make sure data directory exist
        File directory = new File(DATA_PATH_ROOT);
        if (!directory.exists()) //noinspection ResultOfMethodCallIgnored
        {
            directory.mkdir();
        }
    }

    /**
     * Saves objects that implements savable to file.
     *
     * Keep in mind that only one of these works currently, and this should only be used for the apsoulte top level object.
     * @param obj The object to be saved; Must implement savable.
     * @return True if save worked, false if it did not work.
     */
    public SaveResult saveObject(SavableObject obj, String fileName) {
        String pathString = DATA_PATH_ROOT+fileName;

        try {
            //Create writer
            PrintWriter out = new PrintWriter(pathString);
            //Create object that marshals the savable object into xml using the context it provides.
            String data = xmlFactory.convertSavableObject_Full(obj);

            //print data to file
            out.print(data);
            out.close();
        }
        catch(Exception e){
            e.printStackTrace();
            return SaveResult.FAILED_ERROR;
        }

        return SaveResult.SUCCEEDED;
    }


    /**
     * Loads basic save structure from memory at filename.
     * @return BasicSaveStructure object loaded from memory; null if failed
     */
    public BasicSaveStructure loadBasic(String fileName){
        String pathString = DATA_PATH_ROOT+fileName;

        try {
            //Open input stream
            InputStream inputStream = new FileInputStream(pathString);

            //Context that corresponds to root class.
            JAXBContext context = JAXBContext.newInstance(BasicSaveStructure.class);

            //Unmarshaller to handle converting the object.
            Unmarshaller unmarshaller = context.createUnmarshaller();
            BasicSaveStructure basicSaveStructure = (BasicSaveStructure) unmarshaller.unmarshal(inputStream);

            return basicSaveStructure;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * Loads basic save structure from memory at filename.
     * @return BasicSaveStructure object loaded from memory; null if failed
     */
    public Facility loadFacility(String fileName){
        String pathString = DATA_PATH_ROOT+fileName;

        try {
            //Open input stream
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);;

            //Context that corresponds to root class.
            JAXBContext context = JAXBContext.newInstance(Facility.class);

            //Unmarshaller to handle converting the object.
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Facility facility = (Facility) unmarshaller.unmarshal(inputStream);

            return facility;
        }
        catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }

    }


}
