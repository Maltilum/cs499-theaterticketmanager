package model.dataHelpers;

public enum SaveResult {
    SUCCEEDED,
    FAILED_ERROR,
    UPDATED,
    FAILED_EXISTING

}
