package model.dataHelpers;

import model.SavableObject;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

/**
 *  Helper class that converts savable objects to pretty-print xml strings.
 */
public class XMLFactory {


    public XMLFactory(){

        //Disables an old optimization feature that is now illegal in Java.
        System.setProperty("com.sun.xml.bind.v2.bytecode.ClassTailor.noOptimize", "true");

    }

    public String convertAnyObject_Full(Object obj, JAXBContext jaxbContext){
        try {

            //Get Jaxb context from the savable object.
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            //Create string writer to convert the data to a string.
            StringWriter sw = new StringWriter();

            //Convert object to xml and feed it into the stringWriter.
            marshaller.marshal(obj, sw);

            return sw.toString();

        } catch (Exception e){
            e.printStackTrace();
            return "ERROR";
        }
    }
    public String convertAnyObject_Fragment(Object obj, JAXBContext jaxbContext){
        try {

            //Get Jaxb context from the savable object.
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);

            //Create string writer to convert the data to a string.
            StringWriter sw = new StringWriter();

            //Convert object to xml and feed it into the stringWriter.
            marshaller.marshal(obj, sw);

            return sw.toString();

        } catch (Exception e){
            e.printStackTrace();
            return "ERROR";
        }

    }

    public String convertSavableObject_Full(SavableObject obj){

        try {

            //Get Jaxb context from the savable object.
            Marshaller marshaller = obj.getJAXBContext().createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            //Create string writer to convert the data to a string.
            StringWriter sw = new StringWriter();

            //Convert object to xml and feed it into the stringWriter.
            marshaller.marshal(obj, sw);

            return sw.toString();

        } catch (Exception e){
            e.printStackTrace();
            return "ERROR: Conversion Failed";
        }
    }

    public String convertSavableObject_Fragment(SavableObject obj){

        try {

            //Get Jaxb context from the savable object.
            Marshaller marshaller = obj.getJAXBContext().createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            //Shows that it's an XML fragment, meaning not meant to be its own file.
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);

            //Create string writer to convert the data to a string.
            StringWriter sw = new StringWriter();

            //Convert object to xml and feed it into the stringWriter.
            marshaller.marshal(obj, sw);

            return sw.toString();

        } catch (Exception e){
            e.printStackTrace();
            return "ERROR: Conversion Failed";
        }
    }
}
