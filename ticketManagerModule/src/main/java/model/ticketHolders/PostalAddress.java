package model.ticketHolders;

import model.SavableObject;

import javax.xml.bind.annotation.XmlRootElement;


/**
 *  Holds the info for addresses.
 */
@XmlRootElement
public class PostalAddress extends SavableObject {

    private String street;
    private String line2;
    private String city;
    private String region;
    private String zipCode;

    public PostalAddress(){
        super();
    }

    public PostalAddress(String street, String line2, String city, String region, String zipCode) {
        super();
        this.street = street;
        this.line2 = line2;
        this.city = city;
        this.region = region;
        this.zipCode = zipCode;
    }



    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * Tests whether all fields in a PostalAddress are equal
     *
     * @param that The PostalAddress to be compared
     * @return True if all fields are equal
     */
    public boolean allFieldsEqual(PostalAddress that) {
        return this.getCity().equals(that.getCity()) &&
                this.getLine2().equals(that.getLine2()) &&
                this.getRegion().equals(that.getRegion()) &&
                this.getStreet().equals(that.getStreet()) &&
                this.getZipCode().equals(that.getZipCode()) &&
                this.getUuid().equals(that.getUuid());
    }
}
