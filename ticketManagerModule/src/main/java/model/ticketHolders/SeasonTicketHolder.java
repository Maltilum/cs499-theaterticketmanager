package model.ticketHolders;

import presenters.ModelType;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


/**
 * Child class of ticket holder for season ticket holders
 * It includes extra data fields to hold the seat they have reserved.
 * <p>
 * What seat a person has reserved for a show is usually not stored in the holder, but represented by the object structure of the program.
 * <p>
 * In this case though, there needs to be a record of their preferred seat separate from the seat they are actually in since this information must be stored indefinitely.
 */
@XmlRootElement
public class SeasonTicketHolder extends TicketHolder {


    private final ModelType IMPORT_TYPE = ModelType.SEASON_TICKET_HOLDER;
    private String phone_number;
    private PostalAddress address;
    /**
     * List of seating preferences
     * Information is stored as a string in the form of FFF-Sec_#-#
     * FFF = A three character identifier for the facility.
     * - Civic Center playhouse = ccp
     * - Civic Center Concert Hall = cch
     * Sec_# = The Section. Sec is part of the identifier. So Section 1 would be Sec_1
     * The final # = The seat number for that section.
     */
    private List<String> seating_preferences;

    /**
     * Basic constructor that includes all vars.
     *
     * @param name                Name of the ticket holder.
     * @param ticket_price        The price they paid for the ticket.
     * @param has_paid            If they have paid for their ticket in advance or not.
     * @param is_disabled         If the person is disabled.
     * @param disabled_notes      Notes about their disability.
     * @param misc_notes          Other notes if needed
     * @param address             The holder's mailing address
     * @param phone_number        The holder's phone number≤
     * @param seating_preferences A list of strings that encode the season pass holder's seating preferences.
     */
    public SeasonTicketHolder(String name, float ticket_price, boolean has_paid, boolean is_disabled, String disabled_notes, String misc_notes, PostalAddress address, String phone_number, List<String> seating_preferences) {
        super(name, ticket_price, has_paid, is_disabled, disabled_notes, misc_notes);
        this.address = address;
        this.phone_number = phone_number;
        this.seating_preferences = seating_preferences;
    }

    public SeasonTicketHolder(){
        super();
    }


    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public List<String> getSeating_preferences() {
        return seating_preferences;
    }


    /**
     * @param seating_preferences List of seating preferences.
     */
    public void setSeating_preferences(List<String> seating_preferences) {
        this.seating_preferences = seating_preferences;
    }

    public PostalAddress getAddress() {
        return address;
    }

    public void setAddress(PostalAddress address) {
        this.address = address;
    }

    /**
     * Tests whether all fields in a SeasonTicketHolder are equal
     *
     * @param that The SeasonTicketHolder to be compared
     * @return True if all fields are equal
     */
    public boolean allFieldsEqual(SeasonTicketHolder that) {
        return this.getUuid().equals(that.getUuid()) &&
                this.getDisplayName().equals(that.getDisplayName()) &&
                this.getTicketPrice() == that.getTicketPrice() &&
                this.getPaid() == that.getPaid() &&
                this.getDisabled() == that.getDisabled() &&
                this.getDisabledNotes().equals(that.getDisabledNotes()) &&
                this.getMiscNotes().equals(that.getMiscNotes()) &&
                this.getPhone_number().equals(that.getPhone_number()) &&
                ((this.seating_preferences == null && that.getSeating_preferences() == null) || this.getSeating_preferences().equals(that.getSeating_preferences())) &&
                this.getAddress().equals(that.getAddress());
    }

    @Override
    public String toString() {
        return getDisplayName();
    }
}

