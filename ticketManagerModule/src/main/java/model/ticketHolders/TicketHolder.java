package model.ticketHolders;

import model.SavableObject;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * Class that represents a single ticket holder, a single person who is coming to the show.
 */
@SuppressWarnings("ALL")
@XmlRootElement
public class TicketHolder extends SavableObject {



    //Stores how much a user spent on a ticket to facilitate swapping for equally or lesser priced tickets.
    private float ticketPrice;

    //Have they paid for their ticket yet?
    private boolean paid;

    //Are they disabled? Set to false by default so if it is left unset it defaults to the most likely option.
    private boolean disabled;

    //Place to list what disabilities a disabled person has and the accommodations they need.
    private String disabledNotes;

    //Other notes for the user.
    private String miscNotes;

    public TicketHolder() {
        super();
    }

    public TicketHolder(String name){
        super();
        this.setDisplayName(name);
    }

    /**
     * Basic constructor for ticket holder
     *
     * @param name          Name of the ticket holder.
     * @param ticketPrice   The price they paid for the ticket.
     * @param paid          If they have paid for their ticket in advance or not.
     * @param disabled      If the person is disabled.
     * @param disabledNotes Notes about their disability.
     * @param misc_notes    Other notes if needed
     */
    public TicketHolder(String name, float ticketPrice, boolean paid, boolean disabled, String disabledNotes, String misc_notes) {
        super();
        this.ticketPrice = ticketPrice;
        this.paid = paid;
        this.disabled = disabled;
        this.disabledNotes = disabledNotes;
        this.miscNotes = misc_notes;
        this.setDisplayName(name);

    }


    public String getDisplayName() {
        return super.getDisplayName();
    }

    public void setDisplayName(String displayName) {
        super.setDisplayName(displayName);
    }

    public float getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(float ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public boolean getPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getDisabledNotes() {
        return disabledNotes;
    }

    public void setDisabledNotes(String disabledNotes) {
        this.disabledNotes = disabledNotes;
    }

    public String getMiscNotes() {
        return miscNotes;
    }

    public void setMiscNotes(String miscNotes) {
        this.miscNotes = miscNotes;
    }


    /**
     * Tests whether all fields in a TicketHolder are equal
     *
     * @param that The TicketHolder to be compared
     * @return True if all fields are equal
     */
    public boolean allFieldsEqual(TicketHolder that) {

        return this.getUuid().equals(that.getUuid()) &&
                this.getDisplayName().equals(that.getDisplayName()) &&
                this.getTicketPrice() == that.getTicketPrice() &&
                this.getPaid() == that.getPaid() &&
                this.getDisabled() == that.getDisabled() &&
                this.getDisabledNotes().equals(that.getDisabledNotes()) &&
                this.getMiscNotes().equals(that.getMiscNotes());
    }
}
