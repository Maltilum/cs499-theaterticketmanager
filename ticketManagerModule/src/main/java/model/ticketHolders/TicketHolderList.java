package model.ticketHolders;

import model.SavableObject;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "SeasonTicketHolders")
@XmlAccessorType(XmlAccessType.FIELD)
public class TicketHolderList extends SavableObject {

    @XmlElement(name = "SeasonTicketHolder")
    private List<TicketHolder> ticketHolders;

    public TicketHolderList() {
        ticketHolders = new ArrayList<>();
    }

    public List<TicketHolder> getTicketHolders() {
        return ticketHolders;
    }

    public void setTicketHolders(List<TicketHolder> ticketHolders) {
        this.ticketHolders = ticketHolders;
    }

    public void addSeaTicketHold(TicketHolder holder) {

        //Search over the list for the same UUID, and if it's found remove that holder because we treat that as updating data.
        ticketHolders.removeIf(th -> th.getUuid().equals(holder.getUuid()));
        this.ticketHolders.add(holder);
    }

    /**
     * Tests whether all fields in a TicketHolderList are equal
     *
     * @param that The TicketHolderList to be compared
     * @return True if all fields are equal
     */
    public boolean allFieldsEqual(TicketHolderList that) {
        return this.getUuid().equals(that.getUuid()) &&
                this.getTicketHolders().equals(that.getTicketHolders());
    }
}
