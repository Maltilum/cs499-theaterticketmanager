package presenters;

import model.Root;
import model.SavableObject;
import model.dataHelpers.XMLFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import presenters.RequestHelpers.RequestGET;
import presenters.RequestHelpers.RequestXMLContainer;
import presenters.SavableObjectHelpers.SearchHelper;
import presenters.XmlHelpers.RequestUnmarshaller;
import presenters.XmlHelpers.XmlDocumentHelper;
import presenters.response.BasicResponse;
import presenters.response.Response;

import presenters.response.ResponseStatus;
import presenters.response.Response_GET;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import java.io.IOException;

/**
 * Handles communication between view and model.
 */
public class BasicPresenter implements Presenter{


    //The root data structure of for storing what happens where.
    Root root;

    //Used to convert things to xml for sending info back to user interface.
    XMLFactory xmlFactory;

    //Helps search for objects
    private SearchHelper searchHelper;

    //Helps with XML document
    private XmlDocumentHelper xmlDocumentHelper;

    //Helps convert request o useable object
    private RequestUnmarshaller reqUnmarshaller;


    /**
     * Basic constructor
     */
    public BasicPresenter() throws ParserConfigurationException, TransformerConfigurationException, JAXBException {
        root = new Root();
        root.loadData();
        xmlFactory = new XMLFactory();
        searchHelper = new SearchHelper();
        xmlDocumentHelper = new XmlDocumentHelper();
        reqUnmarshaller = new RequestUnmarshaller();

    }



    @Override
    public String request(String request) {

        RequestXMLContainer req;
        try {

           req = reqUnmarshaller.unmarshalBasicReq(request);

        } catch (Exception e) {
            e.printStackTrace();
            BasicResponse failedResponse = new BasicResponse(e.toString(), ResponseStatus.ERROR);
            return failedResponse.getResponseXML();
        }

        if(req == null){
            return new BasicResponse("request type error", ResponseStatus.FAILED).getResponseXML();
        }

        switch(req.requestType){
            case GET:
                return RequestHandlerGet(request);

        }

        return new BasicResponse("reached end of function", ResponseStatus.FAILED).getResponseXML();
    }




    /** Processes GET request.
     * @param req The request string
     * @return The response
     */
    private String RequestHandlerGet(String req){
        try {
            //Marshal the request into an object for ease of access
            RequestGET request = reqUnmarshaller.unmarshallGETReq(req);

            String allData = xmlFactory.convertAnyObject_Full(root, JAXBContext.newInstance(Root.class));

            if(request.query == "all"){
                BasicResponse response = new BasicResponse(allData, ResponseStatus.SUCCEEDED) ;
                return response.getResponseXML();
            } else {
                NodeList resList = xmlDocumentHelper.queryXmlString(allData,  request.query);
                Response response = new Response_GET(resList, ResponseStatus.SUCCEEDED);
                BasicResponse basicResponse = (BasicResponse)response;
                return basicResponse.getResponseXML();
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new BasicResponse(e.toString(), ResponseStatus.ERROR).getResponseXML();
        }
    }
}
