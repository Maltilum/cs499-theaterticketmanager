package presenters;

public enum ModelType {

    NONE,
    TICKET_HOLDER,
    SEASON_TICKET_HOLDER,
    SEAT,
    ROW,
    SECTION,
    FACILITY,
    PERFORMANCE,
    SEASON,
    PRODUCTION,
}
