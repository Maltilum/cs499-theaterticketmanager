package presenters;

import model.*;
import presenters.response.BasicResponse;
import presenters.response.Response;

import javax.xml.bind.annotation.XmlType;
import java.util.List;
import java.util.UUID;

public interface Presenter {

    public String request(String request);

    /*

    public Season get_season(UUID uuid);

    public Production get_production(UUID seasonUUID, UUID productionUUID);

    public Performance get_performance(UUID seasonUUID, UUID PerformanceUUID);

    public Facility get_facility(UUID seasonUUID, UUID productionUUID, UUID performanceUUID);

    public Section get_section(UUID seasonUUID, UUID performanceUUID, UUID sectionUUID);

     */



}
