/* Author: Nicholas Burns
 * Created on: 11/16/2020
 */
package presenters.RequestHelpers;

import model.dataHelpers.XMLFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "request")
public class BasicRequest implements Request {

    @XmlElement
    private String content;

    @XmlAttribute
    private RequestStatus status;

    public BasicRequest(String content, RequestStatus status) {
        this.content = content;
        this.status = status;
    }

    /**
     * Updates the content of the BasicRequest
     *
     * @param content The new String of content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Updates the status of the BasicRequest
     *
     * @param status The new RequestStatus
     */
    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    @XmlTransient
    @Override
    public String getContent() {
        return content;
    }

    @XmlTransient
    @Override
    public RequestStatus getStatus() {
        return status;
    }

    @Override
    public String getRequestXml() {
        XMLFactory xmlFactory = new XMLFactory();

        try {
            return xmlFactory.convertAnyObject_Full(this, JAXBContext.newInstance(this.getClass()));
        }
        catch (JAXBException e) {
            e.printStackTrace();
            return "ERROR: " + e.toString();
        }
    }
}
