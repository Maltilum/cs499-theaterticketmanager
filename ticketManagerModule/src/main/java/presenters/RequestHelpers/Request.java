/* Author: Nicholas Burns
 * Created on: 11/16/2020
 */
package presenters.RequestHelpers;

public interface Request {

    /**
     * Getter method for the content that the request contains
     *
     * @return A string with the information for the request
     */
    public String getContent();

    /**
     * Getter method for the status of the request
     *
     * @return a ResponseStatus enum
     */
    public RequestStatus getStatus();

    /**
     * @return The full data of the request in XML format
     */
    public String getRequestXml();
}
