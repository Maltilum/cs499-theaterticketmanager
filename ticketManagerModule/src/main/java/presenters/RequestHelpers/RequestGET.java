package presenters.RequestHelpers;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name = "request")
public class RequestGET {

    @XmlElement
    public String query;

}
