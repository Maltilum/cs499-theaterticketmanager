package presenters.RequestHelpers;

public enum RequestStatus {
    SUCCEEDED,
    FAILED,
    ERROR
}
