package presenters.RequestHelpers;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement (name = "request")
public class RequestXMLContainer {

    @XmlAttribute (name = "type")
    public RequestType requestType;

    @XmlAnyElement
    public List<Element> other;

    public RequestXMLContainer(){}

    public void setOther(List<Element> others){
        this.other = others;
    }
}
