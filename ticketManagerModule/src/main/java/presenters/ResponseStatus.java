package presenters;

public enum ResponseStatus {
    SUCCEEDED,
    FAILED,
    ERROR,
}
