package presenters.SavableObjectHelpers;

import model.*;

import java.util.List;

public class SearchHelper {

    public SearchHelper(){

    }

    public int findIndexOfUUID(List<? extends SavableObject> list, String uuid){
        for(int i = 0; i<list.size(); i++){
            SavableObject savableObject = list.get(i);
            if(savableObject.getUuid().toString().equals(uuid)){
                return i;
            }
        }

        return -1;
    }

    public SavableObject findObjectFromUUID(List<? extends SavableObject> list, String uuid){

        if(list.size() < 1){
            return null;
        }

        int index = findIndexOfUUID(list, uuid);
        if(index == -1){
            return null;
        }

        return list.get(index);
    }

    public SavableObject findUUIDInList(List<? extends SavableObject> list, String uuid){
        return deepSearchSavableObject(list, uuid);
    }

    private SavableObject deepSearchSavableObject(List<? extends SavableObject> list, String uuid){
        if(list.size()< 1){
            return null;
        }

        Object obj = list.get(0);

        if(obj instanceof Season){
            return deepSearchSeason(list, uuid);
        }
        if(obj instanceof Production){
            return deepSearchProduction(list, uuid);
        }
        if(obj instanceof  Performance){
            return deepSearchPerformance(list, uuid);
        }
        if(obj instanceof  Section){
            return deepSearchSection(list, uuid);
        }
        if(obj instanceof Row){
            deepSearchRow(list, uuid);
        }

        return null;
    }

    private SavableObject deepSearchSeason(List<? extends SavableObject> list, String uuid){

        if(list.size()<1){
            return null;
        }

        if(list.get(0) instanceof Season == false){
            return null;
        }
        for(SavableObject  o:list){
            Season s = (Season)o;
            if(s.getUuid().toString().equals(uuid)){
                return s;
            }

            SavableObject res = deepSearchProduction(s.getProductionsList(), uuid);
            if(res != null){
                return res;
            }

        }
        return null;
    }

    private SavableObject deepSearchProduction(List<? extends  SavableObject> list, String uuid){
        if(list.size()<1){
            return null;
        }

        if(list.get(0) instanceof Production == false){
            return null;
        }
        for(SavableObject o: list){
            Production p  = (Production) o;
            if(p.getUuid().toString().equals(uuid)){
                return p;
            }

            SavableObject res = deepSearchPerformance(p.getPerformances(), uuid);
            if(res != null){
                return res;
            }
        }
        return null;

    }

    private SavableObject deepSearchPerformance(List<? extends SavableObject> list, String uuid){
        if(list.size()<1){
            return null;
        }

        if(list.get(0) instanceof Performance == false){
            return null;
        }
        for(SavableObject o: list){
            Performance p  = (Performance) o;
            if(p.getUuid().toString().equals(uuid)){
                return p;
            }


            SavableObject res = deepSearchFacility(p.getFacility(), uuid);
            if(res != null){
                return res;
            }
        }
        return null;
    }

    private SavableObject deepSearchFacility(Facility f, String uuid){
            if(f.getUuid().toString().equals(uuid)){
                return f;
            }

            SavableObject res = deepSearchSection(f.getSections(), uuid);
            if(res != null){
                return res;
            }

        return null;
    }

    private SavableObject deepSearchSection(List<? extends SavableObject> list, String uuid){

        if(list.size()<1){
            return null;
        }

        if(list.get(0) instanceof Section == false){
            return null;
        }
        for(SavableObject o: list){
            Section s  = (Section) o;
            if(s.getUuid().toString().equals(uuid)){
                return s;
            }
           SavableObject res = deepSearchRow(s.getRows(), uuid);
            if(res != null){
                return res;
            }
        }
        return null;
    }

    private SavableObject deepSearchRow(List<? extends SavableObject> list, String uuid){
        if(list.size()<1){
            return null;
        }

        if(list.get(0) instanceof Row == false){
            return null;
        }
        for(SavableObject o: list){
            Row r  = (Row) o;
            if(r.getUuid().toString().equals(uuid)){
                return r;
            }
            SavableObject seatRes = deepSearchSeat(r.getSeats(), uuid);

            if(seatRes != null){
                return seatRes;
            }
        }

        return null;
    }

    private SavableObject deepSearchSeat(List<? extends SavableObject> list, String uuid){
        if(list.size()<1){
            return null;
        }

        if(list.get(0) instanceof Seat == false){
            return null;
        }
        for(SavableObject o: list){
            Seat s  = (Seat) o;
            if(s.getUuid().toString().equals(uuid)){
                return s;
            }

            if(s.getOccupant().getUuid().toString().equals(uuid)){
                return s.getOccupant();
            }
        }

        return null;
    }



}
