package presenters.XmlHelpers;

import presenters.RequestHelpers.RequestGET;
import presenters.RequestHelpers.RequestXMLContainer;
import presenters.response.BasicResponse;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

public class RequestUnmarshaller {


    private Unmarshaller basicRequestUnmarshaller;
    private Unmarshaller GETReqUnmarshaller;

    public RequestUnmarshaller() throws JAXBException {
        JAXBContext basicRequestContext = JAXBContext.newInstance(RequestXMLContainer.class);
        basicRequestUnmarshaller = basicRequestContext.createUnmarshaller();

        JAXBContext getRequestContext = JAXBContext.newInstance(RequestGET.class);
        GETReqUnmarshaller = getRequestContext.createUnmarshaller();
    }

    /** Converts the XML string of a request to a RequestXMLContainer object.
     * @param xmlString The xml string of the request that one wishes to unmarshal.
     * @return An instance of RequestXMLContainer
     * @throws JAXBException
     */
    public RequestXMLContainer unmarshalBasicReq(String xmlString) throws JAXBException {
        StringReader reader = new StringReader(xmlString);
        RequestXMLContainer request = (RequestXMLContainer) basicRequestUnmarshaller.unmarshal(reader);
        return request;

    }

    public RequestGET unmarshallGETReq(String xmlString) throws JAXBException {
        StringReader reader = new StringReader(xmlString);
        RequestGET request = (RequestGET) GETReqUnmarshaller.unmarshal(reader);
        return request;
    }
}
