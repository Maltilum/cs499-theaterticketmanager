package presenters.XmlHelpers;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

public class XmlDocumentHelper {

    private  DocumentBuilderFactory factory;

    private DocumentBuilder documentBuilder;

    private XPathFactory xPathFactory;

    private Transformer transformer;

    private Transformer transformerFragment;

    public XmlDocumentHelper() throws ParserConfigurationException, TransformerConfigurationException {
        this.factory = DocumentBuilderFactory.newInstance();
        this.documentBuilder = factory.newDocumentBuilder();
        this.xPathFactory = XPathFactory.newInstance();
        this.transformer = TransformerFactory.newInstance().newTransformer();
        this.transformerFragment = TransformerFactory.newInstance().newTransformer();
        transformerFragment.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformerFragment.setOutputProperty(OutputKeys.INDENT, "yes");

    }

    /**
     *  Converts XML strings to DOM documents.
     * @param xmlString The xml string to build a document out of.
     * @return The document
     * @throws IOException
     * @throws SAXException
     */
    public Document docFromString(String xmlString) throws IOException, SAXException {

        StringReader sr = new StringReader(xmlString);
        InputSource is = new InputSource(sr);
        Document result = documentBuilder.parse(is);

        return result;
    }

    /**
     * Query an XML string for specific information
     * @param xmlString
     * @param expression
     * @return
     * @throws XPathExpressionException
     */
    public NodeList queryXmlString(String xmlString, String expression) throws XPathExpressionException {

        XPath xpath = xPathFactory.newXPath();
        InputSource source = new InputSource(new StringReader(xmlString));

        XPathExpression expr = xpath.compile(expression);

        NodeList nodeList = (NodeList)  xpath.evaluate(expression, source, XPathConstants.NODESET);
        return nodeList;
    }

    /**
     * Convert a Dom Node into an XML string
     * @param node An xml "dom" node.
     * @return An xml string of the node.
     * @throws TransformerException
     */
    public String nodeToXmlString(Node node) throws TransformerException {
        StringWriter writer = new StringWriter();
        transformerFragment.transform(new DOMSource(node), new StreamResult(writer));
        return writer.toString();
    }



}
