package presenters.response;

import model.dataHelpers.XMLFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "response")
public class BasicResponse implements Response {

    @XmlElement
    private String content;

    @XmlAttribute
    private ResponseStatus status;

    public BasicResponse(String content, ResponseStatus status){
        this.content = content;
        this.status = status;
    }

    public BasicResponse(){
    }

    @XmlTransient
    public String getContent() {
        return content;
    }

    @XmlTransient
    @Override
    public ResponseStatus getStatus() {
        return status;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    @Override
    public String getResponseXML() {
        XMLFactory xmlFactory = new XMLFactory();
        try {
            return xmlFactory.convertAnyObject_Full(this, JAXBContext.newInstance(this.getClass()));
        } catch (JAXBException e) {
            e.printStackTrace();
            return "ERROR: " + e.toString();
        }
    }
}
