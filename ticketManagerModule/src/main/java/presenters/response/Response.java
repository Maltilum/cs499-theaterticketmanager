package presenters.response;

public interface Response {

    public String getContent();
    public ResponseStatus getStatus();

    /**
     * @return The full data of the response in XML format.
     */
    public String getResponseXML();
}
