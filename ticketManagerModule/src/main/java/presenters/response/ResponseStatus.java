package presenters.response;

public enum ResponseStatus {

    SUCCEEDED,
    FAILED,
    ERROR,
}
