package presenters.response;

import org.w3c.dom.NodeList;
import presenters.XmlHelpers.XmlDocumentHelper;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

@XmlRootElement(name = "response")
public class Response_GET extends BasicResponse {

    public Response_GET(NodeList nodeList, ResponseStatus status) throws ParserConfigurationException, TransformerException {
        super();
        XmlDocumentHelper documentHelper = new XmlDocumentHelper();
        String contentBuffer = "<?xml version=\"1.0\"?>\n";
        for(int i = 0; i<nodeList.getLength(); i++){
            contentBuffer += documentHelper.nodeToXmlString(nodeList.item(i)) + "\n";
        }

        this.setContent(contentBuffer);
        this.setStatus(status);


    }

    public Response_GET(){
        super();
    }
}
