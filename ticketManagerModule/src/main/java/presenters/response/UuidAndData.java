package presenters.response;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

@XmlRootElement
public class UuidAndData {

    private UUID uuid;
    private String data;

    public UuidAndData(UUID uuid, String data){
        this.uuid = uuid;
        this.data = data;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getData() {
        return data;
    }
}
