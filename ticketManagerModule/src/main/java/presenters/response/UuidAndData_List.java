package presenters.response;

import model.SavableObject;
import model.dataHelpers.XMLFactory;

import javax.xml.bind.JAXBException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UuidAndData_List {

    private List<UuidAndData> infoList;

    public UuidAndData_List(){
        infoList = new ArrayList<>();

    }

    public boolean addSavableObject(SavableObject obj){

        XMLFactory xmlFactory = new XMLFactory();
        String data;

        try {
            data = xmlFactory.convertAnyObject_Fragment(obj, obj.getJAXBContext());
        } catch (JAXBException e) {
            e.printStackTrace();
            return false;
        }

        UuidAndData newEntry = new UuidAndData(obj.getUuid(), data);

        return true;

    }

    public String getDataByUuid(UUID uuid){

        for(UuidAndData entry : infoList){
            if(entry.getUuid().compareTo(uuid) == 0){
                return entry.getData();
            }
        }
        return null;
    }
}
