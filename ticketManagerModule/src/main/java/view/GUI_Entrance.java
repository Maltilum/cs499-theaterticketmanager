package view;

import com.opencsv.exceptions.CsvException;
import controller.BasicController;
import model.dataHelpers.CSV_Factory;
import model.ticketHolders.PostalAddress;
import model.ticketHolders.SeasonTicketHolder;
import org.apache.commons.io.FilenameUtils;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GUI_Entrance {
    private JPanel TicketManagerJpane;

    private JLabel MAINSELECTSCREENLabel;

    private JLabel selectSeatLabel;
    private JButton selectSeatButton;

    private JLabel enterAdminAreaLabel;
    private JButton adminAreaButton;

    private JLabel editOrAddTicketLabel;
    private JButton newEditTicketHolderButton;

    private JLabel createNewSeasonLabel;
    private JButton createNewSeasonButton;

    private JLabel editShowsLabel;
    private JButton editShowsButton;

    private JLabel editPricesOfShowsLabel;
    private JButton editPriceButton;

    private JLabel exportSTHLabel;
    private JButton exportSTHButton;

    private JButton exitButton;
    private JLabel importSthLabel;
    private JButton importSthButton;

    private BasicController controller;

    public GUI_Entrance() {
        //These fields are not shown unless the admin button is clicked
        editOrAddTicketLabel.setVisible(false);
        newEditTicketHolderButton.setVisible(false);

        createNewSeasonLabel.setVisible(false);
        createNewSeasonButton.setVisible(false);

        editShowsLabel.setVisible(false);
        editShowsButton.setVisible(false);

        editPricesOfShowsLabel.setVisible(false);
        editPriceButton.setVisible(false);

        exportSTHButton.setVisible(false);
        exportSTHLabel.setVisible(false);

        importSthButton.setVisible(false);
        importSthLabel.setVisible(false);

        this.controller = new BasicController();

        //Add item listener for each of the buttons

        selectSeatButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame("Create seat selector GUI");
                frame.setContentPane(new seat_Selector(controller).seatSelectorlabel);
                frame.pack();
                frame.setVisible(true);
            }
        });


        // Enter Admin Area reveals all hidden buttons
        adminAreaButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                editOrAddTicketLabel.setVisible(true);
                newEditTicketHolderButton.setVisible(true);

                createNewSeasonLabel.setVisible(true);
                createNewSeasonButton.setVisible(true);

                editShowsLabel.setVisible(true);
                editShowsButton.setVisible(true);

                editPricesOfShowsLabel.setVisible(true);
                editPriceButton.setVisible(true);

                exportSTHButton.setVisible(true);
                exportSTHLabel.setVisible(true);

                importSthButton.setVisible(true);
                importSthLabel.setVisible(true);
            }
        });


        //Opens correct GUI
        newEditTicketHolderButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame("Create Ticket Holder GUI");
                frame.setContentPane(new ticket_holder_info(controller).ticket_holder_info);
                frame.pack();
                frame.setVisible(true);
            }
        });

        //Opens correct GUI
        createNewSeasonButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame("Create Season Editor");
                frame.setContentPane(new create_season_productions(controller).create_season_productions);
                frame.pack();
                frame.setVisible(true);
            }
        });

        //Opens correct GUI
        editShowsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame("Create Performance Editor");
                frame.setContentPane(new create_season_performances(controller).create_season_performances);
                frame.pack();
                frame.setVisible(true);
            }
        });

        //Opens correct GUI
        editPriceButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame("Create Price Editor");
                frame.setContentPane(new create_season_price(controller).create_season_price);
                frame.pack();
                frame.setVisible(true);
            }
        });

        exportSTHButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exportSthToCsv();
            }
        });

        importSthButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                importSthFromCsv();
            }
        });

        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

    }

    /**
     * Handles exporting a csv File
     */
    private void exportSthToCsv() {
        //Create and open the window to save the csv file
        CSV_Factory csv_factory = new CSV_Factory();
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV Files", "csv");
        chooser.setFileFilter(filter);

        int returnVal = chooser.showSaveDialog(new JFrame());
        String chosenPath;
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            chosenPath = chooser.getSelectedFile().getAbsolutePath();
            System.out.println("You chose to save this file: " +
                    chosenPath);
            File file = chooser.getSelectedFile();

            try {
                //Ensure that file has the proper extension
                if (FilenameUtils.getExtension(file.getName()).equalsIgnoreCase("csv")) {
                    csv_factory.exportAddressList(controller.get_root_object().getSeasonTicketHolders(), chosenPath);
                }
                else {
                    file = new File(file.getParentFile(), FilenameUtils.getBaseName(file.getName()) + ".csv"); // ALTERNATIVELY: remove the extension (if any) and replace it with ".xml"
                    csv_factory.exportAddressList(controller.get_root_object().getSeasonTicketHolders(), file.getAbsolutePath());
                }
            }
            catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    /**
     * Imports a list of SeasonTicketHolders
     */
    private void importSthFromCsv() {
        //Create and open the window to save the csv file
        CSV_Factory csv_factory = new CSV_Factory();
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV Files", "csv");
        chooser.setFileFilter(filter);

        int returnVal = chooser.showOpenDialog(new JFrame());
        String chosenPath;
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            chosenPath = chooser.getSelectedFile().getAbsolutePath();
            System.out.println("You chose to save this file: " +
                    chosenPath);
            File file = chooser.getSelectedFile();
            ArrayList<SeasonTicketHolder> seasonTicketHolders = new ArrayList<>();
            try {
                //Ensure that file has the proper extension
                if (FilenameUtils.getExtension(file.getName()).equalsIgnoreCase("csv")) {
                     seasonTicketHolders = csv_factory.importAddressListBasic(chosenPath);
                }
            }
            catch (IOException | CsvException ex) {
                ex.printStackTrace();
            }

            for (SeasonTicketHolder seasonTicketHolder : seasonTicketHolders) {
                controller.get_root_object().addExistingSeasonTicketHolder(seasonTicketHolder);
            }
            controller.save_to_memory();
        }
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("Entrance GUI");
        frame.setContentPane(new GUI_Entrance().TicketManagerJpane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }


}