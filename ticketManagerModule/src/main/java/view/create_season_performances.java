package view;

import controller.BasicController;
import controller.FacilityName;
import model.Performance;
import model.Production;
import model.Season;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class create_season_performances {
    private static final int MAX_PERFORMANCES = 6;
    private static final Date MIN_DATE = new Date(120, 0, 1, 8, 0);

    public JPanel create_season_performances;

    private JLabel selectSeasonLabel;
    private JList select_season_list;

    private JLabel selectProductionLabel;
    private JList select_production_list;

    private JLabel monthLabel;
    private JLabel dayLabel;
    private JLabel yearLabel;
    private JLabel timeLabel;
    private JLabel facilityLabel;

    private JLabel show1DateLabel;
    private JLabel show1TimeLabel;
    private JList show_1_month;
    private JList show_1_day;
    private JList show_1_year;
    private JList show_1_time;
    private JList show_1_facillity;

    private JLabel show2DateLabel;
    private JLabel show2TimeLabel;
    private JList show_2_month;
    private JList show_2_day;
    private JList show_2_year;
    private JList show_2_time;
    private JList show_2_facillity;

    private JLabel show3DateLabel;
    private JLabel show3TimeLabel;
    private JList show_3_month;
    private JList show_3_day;
    private JList show_3_year;
    private JList show_3_time;
    private JList show_3_facillity;


    private JLabel show4DateLabel;
    private JLabel show4TimeLabel;
    private JList show_4_month;
    private JList show_4_day;
    private JList show_4_year;
    private JList show_4_time;
    private JList show_4_facillity;


    private JLabel show5DateLabel;
    private JLabel show5TimeLabel;
    private JList show_5_month;
    private JList show_5_day;
    private JList show_5_year;
    private JList show_5_time;
    private JList show_5_facillity;

    private JLabel show6DateLabel;
    private JLabel show6TimeLabel;
    private JList show_6_month;
    private JList show_6_day;
    private JList show_6_year;
    private JList show_6_time;
    private JList show_6_facillity;

    private JButton moveToPriceEditorButton;
    private JButton saveButton;
    private JLabel dataSavedLabel;

    private ArrayList<JLabel> showDateLabels = new ArrayList<>();
    private ArrayList<JLabel> showTimeLabels = new ArrayList<>();
    private ArrayList<JList> showMonthLists = new ArrayList<>();
    private ArrayList<JList> showDayLists = new ArrayList<>();
    private ArrayList<JList> showYearLists = new ArrayList<>();
    private ArrayList<JList> showTimeLists = new ArrayList<>();
    private ArrayList<JList> showFacilityLists = new ArrayList<>();

    private BasicController controller;

    create_season_performances(BasicController controller) {
        this.controller = controller;

        populateSeasons();

        showDateLabels.add(show1DateLabel);
        showDateLabels.add(show2DateLabel);
        showDateLabels.add(show3DateLabel);
        showDateLabels.add(show4DateLabel);
        showDateLabels.add(show5DateLabel);
        showDateLabels.add(show6DateLabel);
        for (JLabel showDateLabel : showDateLabels) {
            showDateLabel.setVisible(false);
        }

        showTimeLabels.add(show1TimeLabel);
        showTimeLabels.add(show2TimeLabel);
        showTimeLabels.add(show3TimeLabel);
        showTimeLabels.add(show4TimeLabel);
        showTimeLabels.add(show5TimeLabel);
        showTimeLabels.add(show6TimeLabel);
        for (JLabel showTimeLabel : showTimeLabels) {
            showTimeLabel.setVisible(false);
        }

        showMonthLists.add(show_1_month);
        showMonthLists.add(show_2_month);
        showMonthLists.add(show_3_month);
        showMonthLists.add(show_4_month);
        showMonthLists.add(show_5_month);
        showMonthLists.add(show_6_month);
        for (JList monthList : showMonthLists) {
            monthList.setVisible(false);
            monthList.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    updateDaysInMonth();
                }
            });
        }

        showDayLists.add(show_1_day);
        showDayLists.add(show_2_day);
        showDayLists.add(show_3_day);
        showDayLists.add(show_4_day);
        showDayLists.add(show_5_day);
        showDayLists.add(show_6_day);
        for (JList dayList : showDayLists) {
            dayList.setVisible(false);
        }

        showYearLists.add(show_1_year);
        showYearLists.add(show_2_year);
        showYearLists.add(show_3_year);
        showYearLists.add(show_4_year);
        showYearLists.add(show_5_year);
        showYearLists.add(show_6_year);
        for (JList yearList : showYearLists) {
            yearList.setVisible(false);
            yearList.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    updateDaysInMonth();
                }
            });
        }

        showTimeLists.add(show_1_time);
        showTimeLists.add(show_2_time);
        showTimeLists.add(show_3_time);
        showTimeLists.add(show_4_time);
        showTimeLists.add(show_5_time);
        showTimeLists.add(show_6_time);
        for (JList timeList : showTimeLists) {
            timeList.setVisible(false);
        }

        showFacilityLists.add(show_1_facillity);
        showFacilityLists.add(show_2_facillity);
        showFacilityLists.add(show_3_facillity);
        showFacilityLists.add(show_4_facillity);
        showFacilityLists.add(show_5_facillity);
        showFacilityLists.add(show_6_facillity);
        for (JList facilityList : showFacilityLists) {
            facilityList.setVisible(false);
            facilityList.setListData(FacilityName.values());
        }

        selectProductionLabel.setVisible(false);
        select_production_list.setVisible(false);

        monthLabel.setVisible(false);
        dayLabel.setVisible(false);
        yearLabel.setVisible(false);
        timeLabel.setVisible(false);
        facilityLabel.setVisible(false);

        moveToPriceEditorButton.setVisible(true);

        dataSavedLabel.setVisible(false);
        saveButton.setVisible(true);

        //Send season to backend to get productions in the season in list form
        select_season_list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onSeasonSelected();
            }
        });

        //Season production name to backend, get the number of shows in production
        select_production_list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onProductionSelected();
            }
        });

        //NOTHING FROM BACKEND NEEDED HERE
        moveToPriceEditorButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame("Create Price Editor GUI");
                frame.setContentPane(new create_season_price(controller).create_season_price);
                frame.pack();
                frame.setVisible(true);
            }
        });

        //Send season, production, and show info to backend
        //Note- this also could be improved
        saveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onSave();
            }
        });

    }

    /**
     * Handles season being selected from the list of seasons
     */
    private void onSeasonSelected() {
        Season season = (Season) select_season_list.getSelectedValue();

        //Ensures that the value was not changed to null
        if (season == null) {
            selectProductionLabel.setVisible(false);
            select_production_list.setVisible(false);
            return;
        }

        select_production_list.setListData(season.getProductionsList().toArray());
        selectProductionLabel.setVisible(true);
        select_production_list.setVisible(true);
    }

    /**
     * Handles a Production being selected from the list
     */
    private void onProductionSelected() {
        //get the selected production
        Production production = (Production) select_production_list.getSelectedValue();

        if (production == null) {
            monthLabel.setVisible(false);
            dayLabel.setVisible(false);
            yearLabel.setVisible(false);
            timeLabel.setVisible(false);
            facilityLabel.setVisible(false);
            showDayLists.forEach(jList -> jList.setVisible(false));
            showMonthLists.forEach(jList -> jList.setVisible(false));
            showYearLists.forEach(jList -> jList.setVisible(false));
            showTimeLists.forEach(jList -> jList.setVisible(false));
            showFacilityLists.forEach(jList -> jList.setVisible(false));
            return;
        }

        //get number of shows in production int from backend
        ArrayList<Performance> performances = production.getPerformances();

        monthLabel.setVisible(true);
        dayLabel.setVisible(true);
        yearLabel.setVisible(true);
        timeLabel.setVisible(true);
        facilityLabel.setVisible(true);

        for (int counter = 0; counter < MAX_PERFORMANCES; counter++) {
            if (counter < performances.size()) {
                Performance performance = performances.get(counter);
                Date date = performance.getDateAndTime();
                showMonthLists.get(counter).setVisible(true);
                showDayLists.get(counter).setVisible(true);
                showYearLists.get(counter).setVisible(true);
                showTimeLists.get(counter).setVisible(true);
                showFacilityLists.get(counter).setVisible(true);
                showFacilityLists.get(counter).setSelectedValue(performance.getFacilityName(), false);

                if (date != null && date.after(MIN_DATE) && date.getHours() > 7 && date.getHours() < 22) {
                    showMonthLists.get(counter).setSelectedIndex(date.getMonth());
                    showDayLists.get(counter).setSelectedIndex(date.getDate() - 1);
                    showYearLists.get(counter).setSelectedIndex(date.getYear() - 120);
                    showTimeLists.get(counter).setSelectedIndex(date.getHours() - 8);
                }
            }
            else {
                showMonthLists.get(counter).setVisible(false);
                showDayLists.get(counter).setVisible(false);
                showYearLists.get(counter).setVisible(false);
                showTimeLists.get(counter).setVisible(false);
                showFacilityLists.get(counter).setVisible(false);
            }
        }
    }

    /**
     * Handles saving the performances
     */
    private void onSave() {
        if (select_season_list.getSelectedValue() == null) {
            return;
        }

        //get the selected production
        Production production = (Production) select_production_list.getSelectedValue();

        //get number of shows in production int from backend
        ArrayList<Performance> performances = production.getPerformances();

        for (int counter = 0; counter < performances.size(); counter++) {
            int showMonth = showMonthLists.get(counter).getSelectedIndex();
            int showDay = showDayLists.get(counter).getSelectedIndex() + 1;
            int showYear = showYearLists.get(counter).getSelectedIndex() + 2020;
            int showTime = showTimeLists.get(counter).getSelectedIndex() + 8;
            FacilityName showFacility = (FacilityName) showFacilityLists.get(counter).getSelectedValue();

            Date date = new Date(showYear - 1900, showMonth, showDay, showTime, 0);
            performances.get(counter).setDateAndTime(date);
            FacilityName performanceFacility = performances.get(counter).getFacilityName();

            if (!performanceFacility.equals(showFacility)) {
                performances.get(counter).setFacilityName(showFacility);
                if (showFacility.equals(FacilityName.PLAYHOUSE)) {
                    performances.get(counter).setFacility(controller.get_root_object().loadPlayHouse());
                }
                else if (showFacility.equals(FacilityName.CONCERT_HALL)) {
                    performances.get(counter).setFacility(controller.get_root_object().loadConcertHall());
                }
            }
            performances.get(counter).setFacilityName(showFacility);
        }

        controller.save_to_memory();
        dataSavedLabel.setVisible(true);
    }

    /**
     * Populates the list of seasons
     */
    private void populateSeasons() {
        //get season list from backend
        ArrayList<Season> seasons = (ArrayList<Season>) this.controller.get_root_object().getSeasonsList();
        if (seasons != null) {
            select_season_list.setListData(seasons.toArray());
        }
        else {
            return;
        }
    }

    /**
     * Updates the range in the month field
     */
    private void updateDaysInMonth() {
        Integer[] daysInMonth = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};
        for (int i = 0; i < showYearLists.size(); i++) {
            int maxDays = 31;

            int showMonth = showMonthLists.get(i).getSelectedIndex();
            int showYear = showYearLists.get(i).getSelectedIndex() + 2020;
            if (showMonth >= 0 && showYear >= 0) {
                showYear = showYear + 2020;
                Calendar calendar = Calendar.getInstance();
                calendar.set(showYear, showMonth, 1);
                maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            }
            else if (showMonth >= 0) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(2020, showMonth, 1);
                maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            }
            Integer[] subsetDays = Arrays.copyOfRange(daysInMonth, 0, maxDays);

            showDayLists.get(i).setListData(subsetDays);
        }

    }

}
