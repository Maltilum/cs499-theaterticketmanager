package view;

import controller.BasicController;
import controller.FacilityName;
import model.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class create_season_price {
    public JPanel create_season_price;
    private JList select_season_list;
    private JList select_production_list;
    private JList select_show_list;
    private JLabel selectSeasonLabel;
    private JLabel selectProductionLabel;
    private JLabel selectShowLabel;
    private JTextField price_for_all_seats_text;
    private JLabel price_type_label;
    private JList price_type_list;
    private JLabel priceForAllSeatsLabel;
    private JLabel priceForSectionLabel;
    private JLabel priceForRowLabel;
    private JLabel selectSectionLabel;
    private JList select_section_list;
    private JLabel selectRowLabel;
    private JList select_row_list;
    private JTextArea priceTextArea;
    private JButton savePriceButton;
    private JLabel setPriceLabel;
    private JLabel priceSavedLabe;

    private BasicController controller;

    public create_season_price(BasicController controller) {
        //Get Season information from backend
        this.controller = controller;

        populateSeasonsList();

        //hide everything until season has selected.
        selectProductionLabel.setVisible(false);
        select_production_list.setVisible(false);

        selectShowLabel.setVisible(false);
        select_show_list.setVisible(false);

        price_type_label.setVisible(false);
        price_type_list.setVisible(false);

        priceForAllSeatsLabel.setVisible(false);
        price_for_all_seats_text.setVisible(false);

        setPriceLabel.setVisible(false);
        priceForSectionLabel.setVisible(false);
        select_section_list.setVisible(false);
        selectSectionLabel.setVisible(false);


        priceForRowLabel.setVisible(false);
        selectRowLabel.setVisible(false);
        select_row_list.setVisible(false);

        priceTextArea.setVisible(false);

        savePriceButton.setVisible(false);
        priceSavedLabe.setVisible(false);

        //Send season information to backend to get production information
        select_season_list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onSeasonSelected();
            }
        });

        //Send Production information to backend to recieve show list
        select_production_list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onProductionSelected();
            }
        });

        //NOTHING FROM BACKEND NEEDED HERE
        select_show_list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onPerformanceSelected();
            }
        });

        //Send the show information to the backend to recieve the section info
        price_type_list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onPriceTypeSelected();
            }
        });

        //Need to send section to backend to recieve row data here
        select_section_list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onSectionSelected();
            }
        });

        //Nothing from backend needed here
        select_row_list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                savePriceButton.setVisible(true);
                setPriceLabel.setVisible(true);
                priceTextArea.setVisible(true);
            }
        });

        //Sending the following information to backend Season, Production, Show, Section, Row, Price
        savePriceButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onSave();
            }
        });

    }

    /**
     * Handles a Season being selected
     */
    private void onSeasonSelected() {
        Season season = (Season) select_season_list.getSelectedValue();
        //send season to backend

        if (season == null) {
            selectProductionLabel.setVisible(false);
            select_production_list.setVisible(false);
        }

        ArrayList<Production> production_list = (ArrayList<Production>) season.getProductionsList();

        select_production_list.setListData(production_list.toArray());
        selectProductionLabel.setVisible(true);
        select_production_list.setVisible(true);

        selectShowLabel.setVisible(false);
        select_show_list.setVisible(false);
        price_type_label.setVisible(false);
        price_type_list.setVisible(false);
        priceForAllSeatsLabel.setVisible(false);
        price_for_all_seats_text.setVisible(false);
        setPriceLabel.setVisible(false);
        priceForSectionLabel.setVisible(false);
        select_section_list.setVisible(false);
        selectSectionLabel.setVisible(false);
        priceForRowLabel.setVisible(false);
        selectRowLabel.setVisible(false);
        select_row_list.setVisible(false);
        priceTextArea.setVisible(false);
        savePriceButton.setVisible(false);
        priceSavedLabe.setVisible(false);
    }

    /**
     * Handles a Production being selected
     */
    private void onProductionSelected() {
        Production production = (Production) select_production_list.getSelectedValue();

        if (production == null) {
            selectShowLabel.setVisible(false);
            select_show_list.setVisible(false);
            return;
        }

        //recieve shows from backend
        //note show names should be dates in mm/dd/yyyy
        ArrayList<Performance> performances = production.getPerformances();
        select_show_list.setListData(performances.toArray());

        selectShowLabel.setVisible(true);
        select_show_list.setVisible(true);
        price_type_label.setVisible(false);
        price_type_list.setVisible(false);
        priceForAllSeatsLabel.setVisible(false);
        price_for_all_seats_text.setVisible(false);
        setPriceLabel.setVisible(false);
        priceForSectionLabel.setVisible(false);
        select_section_list.setVisible(false);
        selectSectionLabel.setVisible(false);
        priceForRowLabel.setVisible(false);
        selectRowLabel.setVisible(false);
        select_row_list.setVisible(false);
        priceTextArea.setVisible(false);
        savePriceButton.setVisible(false);
        priceSavedLabe.setVisible(false);
    }

    private void onPerformanceSelected() {
        Performance performance = (Performance) select_show_list.getSelectedValue();

        if (performance == null) {
            price_type_label.setVisible(false);
            price_type_list.setVisible(false);
            return;
        }

        price_type_label.setVisible(true);
        price_type_list.setVisible(true);

        priceForAllSeatsLabel.setVisible(false);
        price_for_all_seats_text.setVisible(false);
        setPriceLabel.setVisible(false);
        priceForSectionLabel.setVisible(false);
        select_section_list.setVisible(false);
        selectSectionLabel.setVisible(false);
        priceForRowLabel.setVisible(false);
        selectRowLabel.setVisible(false);
        select_row_list.setVisible(false);
        priceTextArea.setVisible(false);
        savePriceButton.setVisible(false);
        priceSavedLabe.setVisible(false);
    }

    /**
     * Handles a method of pricing being selected
     */
    private void onPriceTypeSelected() {
        Performance performance = (Performance) select_show_list.getSelectedValue();
        //send show to backend

        if (performance.getFacility() == null || performance.getFacilityName().equals(FacilityName.EMPTY)) {
            priceForAllSeatsLabel.setVisible(false);
            price_for_all_seats_text.setVisible(false);
            setPriceLabel.setVisible(false);
            priceForSectionLabel.setVisible(false);
            select_section_list.setVisible(false);
            selectSectionLabel.setVisible(false);
            priceForRowLabel.setVisible(false);
            selectRowLabel.setVisible(false);
            select_row_list.setVisible(false);
            priceTextArea.setVisible(false);
            savePriceButton.setVisible(false);
            priceSavedLabe.setVisible(false);
            return;
        }

        //Get the section list from backend
        ArrayList<Section> sections = performance.getFacility().getSections();
        select_section_list.setListData(sections.toArray());

        //Nothing else needed here
        priceForAllSeatsLabel.setVisible(false);
        price_for_all_seats_text.setVisible(false);
        setPriceLabel.setVisible(false);
        priceForSectionLabel.setVisible(false);
        select_section_list.setVisible(false);
        selectSectionLabel.setVisible(false);
        priceForRowLabel.setVisible(false);
        selectRowLabel.setVisible(false);
        select_row_list.setVisible(false);
        priceTextArea.setVisible(false);
        savePriceButton.setVisible(false);
        priceSavedLabe.setVisible(false);
        //0 is price by show
        //1 is price by section
        //2 is price by row
        int price_type = price_type_list.getSelectedIndex();
        if (price_type == 0) {
            priceSavedLabe.setVisible(false);

            priceForAllSeatsLabel.setVisible(true);
            price_for_all_seats_text.setVisible(true);
            savePriceButton.setVisible(true);
        }
        if (price_type == 1 || price_type == 2) {
            setPriceLabel.setVisible(true);
            priceForSectionLabel.setVisible(true);
            select_section_list.setVisible(true);
            priceForSectionLabel.setVisible(true);
            savePriceButton.setVisible(true);
            setPriceLabel.setVisible(true);
            priceTextArea.setVisible(true);
        }
        if (price_type == 2) {
            setPriceLabel.setVisible(true);
            priceForSectionLabel.setVisible(true);
            select_section_list.setVisible(true);
            select_row_list.setVisible(true);
            priceForSectionLabel.setVisible(true);
            priceForRowLabel.setVisible(true);
        }
    }

    /**
     * Handles a section being selected
     */
    private void onSectionSelected() {
        Section section = (Section) select_section_list.getSelectedValue();
        //send section to backend and recieve corresponding rows
        ArrayList<Row> rows = section.getRows();
        select_row_list.setListData(rows.toArray());
    }

    private void onSave() {
        priceSavedLabe.setVisible(true);
        //Send Season,production, and show to backend
        Performance performance = (Performance) select_show_list.getSelectedValue();

        int price_type = price_type_list.getSelectedIndex();
        if (price_type == 0) {
            //send price and populate all seats in the show with the single price
            float price = Float.parseFloat(price_for_all_seats_text.getText());
            ArrayList<Section> sections = performance.getFacility().getSections();
            for (Section section : sections) {
                section.setSectionPrice(price);
            }
        }
        if (price_type == 1) {
            //send the section to backend and set all seats in the section the price
            Section section = (Section) select_section_list.getSelectedValue();
            float price = Float.parseFloat(priceTextArea.getText());
            section.setSectionPrice(price);
        }
        if (price_type == 2) {
            //send the section and row info to backend, price all seats in the selected row
            Row row = (Row) select_row_list.getSelectedValue();
            float price = Float.parseFloat(priceTextArea.getText());
            row.setPrice(price);
        }
        controller.save_to_memory();
    }

    /**
     * Populates the list of seasons
     */
    private void populateSeasonsList() {
        ArrayList<Season> seasons = (ArrayList<Season>) this.controller.get_root_object().getSeasonsList();
        if (seasons != null) {
            select_season_list.setListData(seasons.toArray());
        }
        else {
            return;
        }
    }
}
