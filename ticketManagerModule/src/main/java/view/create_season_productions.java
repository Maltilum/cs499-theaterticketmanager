package view;

import controller.BasicController;
import controller.ObjectType;
import model.Production;
import model.Season;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class create_season_productions {
    private static final int MAX_PRODUCTIONS = 12;

    public JPanel create_season_productions;

    private JButton create_new_season_button;

    private JLabel select_season_label;
    private JList select_season_list;

    private JLabel seasonNameLabel;
    private JTextArea season_name;

    private JLabel numberOfProductionsIntLabel;
    private JList num_productions_list;

    private JLabel production1NameLabel;
    private JTextArea production1NameString;
    private JLabel numberOfShowsIn1Label;
    private JList shows_in_production_1_int;

    private JLabel production2NameLabel;
    private JTextArea production2NameString;
    private JLabel numberOfShowsIn2Label;
    private JList shows_in_production_2_int;

    private JLabel production3NameLabel;
    private JTextArea production3NameString;
    private JLabel numberOfShowsIn3Label;
    private JList shows_in_production_3_int;

    private JLabel production4NameLabel;
    private JTextArea production4NameString;
    private JLabel numberOfShowsIn4Label;
    private JList shows_in_production_4_int;

    private JLabel production5NameLabel;
    private JTextArea production5NameString;
    private JLabel numberOfShowsIn5Label;
    private JList shows_in_production_5_int;

    private JLabel production6NameLabel;
    private JTextArea production6NameString;
    private JLabel numberOfShowsIn6Label;
    private JList shows_in_production_6_int;

    private JLabel production7NameLabel;
    private JTextArea production7NameString;
    private JLabel numberOfShowsIn7Label;
    private JList shows_in_production_7_int;

    private JLabel production8NameLabel;
    private JTextArea production8NameString;
    private JLabel numberOfShowsIn8Label;
    private JList shows_in_production_8_int;

    private JLabel production9NameLabel;
    private JTextArea production9NameString;
    private JLabel numberOfShowsIn9Label;
    private JList shows_in_production_9_int;

    private JLabel production10NameLabel;
    private JTextArea production10NameString;
    private JLabel numberOfShowsIn10Label;
    private JList shows_in_production_10_int;

    private JLabel production11NameLabel;
    private JTextArea production11NameString;
    private JLabel numberOfShowsIn11Label;
    private JList shows_in_production_11_int;

    private JLabel production12NameLabel;
    private JTextArea production12NameString;
    private JLabel numberOfShowsIn12Label;
    private JList shows_in_production_12_int;

    private JButton moveToShowEditorButton;

    private JButton saveButton;
    private JLabel savedLabel;
    private JTextArea Warning_num_changed;

    private boolean newSeason = false;
    private ArrayList<JTextArea> productionNameTextBoxes = new ArrayList<>();
    private ArrayList<JLabel> productionNameLabels = new ArrayList<>();
    private ArrayList<JList> showsInProductionLists = new ArrayList<>();
    private ArrayList<JLabel> showsInProductionsLabels = new ArrayList<>();

    private BasicController controller;

    /**
     * View for creating and editing seasons
     *
     * @param controller The controller for the view
     */
    create_season_productions(BasicController controller) {
        //Pull season information from backend in list form
        this.controller = controller;

        populateSeasons();

        productionNameTextBoxes.add(production1NameString);
        productionNameTextBoxes.add(production2NameString);
        productionNameTextBoxes.add(production3NameString);
        productionNameTextBoxes.add(production4NameString);
        productionNameTextBoxes.add(production5NameString);
        productionNameTextBoxes.add(production6NameString);
        productionNameTextBoxes.add(production7NameString);
        productionNameTextBoxes.add(production8NameString);
        productionNameTextBoxes.add(production9NameString);
        productionNameTextBoxes.add(production10NameString);
        productionNameTextBoxes.add(production11NameString);
        productionNameTextBoxes.add(production12NameString);

        for (JTextArea textArea : productionNameTextBoxes) {
            textArea.setVisible(false);
        }

        productionNameLabels.add(production1NameLabel);
        productionNameLabels.add(production2NameLabel);
        productionNameLabels.add(production3NameLabel);
        productionNameLabels.add(production4NameLabel);
        productionNameLabels.add(production5NameLabel);
        productionNameLabels.add(production6NameLabel);
        productionNameLabels.add(production7NameLabel);
        productionNameLabels.add(production8NameLabel);
        productionNameLabels.add(production9NameLabel);
        productionNameLabels.add(production10NameLabel);
        productionNameLabels.add(production11NameLabel);
        productionNameLabels.add(production12NameLabel);

        for (JLabel nameLabel : productionNameLabels) {
            nameLabel.setVisible(false);
        }

        showsInProductionsLabels.add(numberOfShowsIn1Label);
        showsInProductionsLabels.add(numberOfShowsIn2Label);
        showsInProductionsLabels.add(numberOfShowsIn3Label);
        showsInProductionsLabels.add(numberOfShowsIn4Label);
        showsInProductionsLabels.add(numberOfShowsIn5Label);
        showsInProductionsLabels.add(numberOfShowsIn6Label);
        showsInProductionsLabels.add(numberOfShowsIn7Label);
        showsInProductionsLabels.add(numberOfShowsIn8Label);
        showsInProductionsLabels.add(numberOfShowsIn9Label);
        showsInProductionsLabels.add(numberOfShowsIn10Label);
        showsInProductionsLabels.add(numberOfShowsIn11Label);
        showsInProductionsLabels.add(numberOfShowsIn12Label);

        for (JLabel showsLabel : showsInProductionsLabels) {
            showsLabel.setVisible(false);
        }

        showsInProductionLists.add(shows_in_production_1_int);
        showsInProductionLists.add(shows_in_production_2_int);
        showsInProductionLists.add(shows_in_production_3_int);
        showsInProductionLists.add(shows_in_production_4_int);
        showsInProductionLists.add(shows_in_production_5_int);
        showsInProductionLists.add(shows_in_production_6_int);
        showsInProductionLists.add(shows_in_production_7_int);
        showsInProductionLists.add(shows_in_production_8_int);
        showsInProductionLists.add(shows_in_production_9_int);
        showsInProductionLists.add(shows_in_production_10_int);
        showsInProductionLists.add(shows_in_production_11_int);
        showsInProductionLists.add(shows_in_production_12_int);

        for (JList numShowsList : showsInProductionLists) {
            numShowsList.setVisible(false);
        }

        seasonNameLabel.setVisible(false);
        season_name.setVisible(false);

        numberOfProductionsIntLabel.setVisible(false);
        num_productions_list.setVisible(false);

        Warning_num_changed.setVisible(false);

        savedLabel.setVisible(false);
        saveButton.setVisible(true);

        //THERE IS NOTHING THE BACKEND NEEDS HERE
        moveToShowEditorButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame("Create Show Editor GUI");
                frame.setContentPane(new create_season_performances(controller).create_season_performances);
                frame.pack();
                frame.setVisible(true);
            }
        });


        //THERE IS NOTHING THE BACKEND NEEDS HERE
        create_new_season_button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCreateNewSeason();
            }
        });

        //Get season name, number in productions, and the name of each production via list form
        select_season_list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onSeasonSelected();
            }
        });


        //Send Season name, number of productions in season, name of each production,
        // and number of show in each production
        //Note- this code could be made better, feel free to edit.
        saveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveSeason();
            }
        });


    }

    /**
     * Sets up the view to create a new season
     */
    private void onCreateNewSeason() {
        newSeason = true;
        select_season_label.setVisible(false);
        select_season_list.setVisible(false);

        seasonNameLabel.setVisible(true);
        season_name.setVisible(true);

        numberOfProductionsIntLabel.setVisible(true);
        num_productions_list.setVisible(true);

        num_productions_list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onNumProductionsChanged();
            }
        });
    }

    /**
     * Handles the change in number of productions selected
     */
    private void onNumProductionsChanged() {
        int prod_num = num_productions_list.getSelectedIndex() + 1;

        //Display the selected number of productions
        for (int counter = 0; counter < MAX_PRODUCTIONS; counter++) {
            if (counter < prod_num) {
                productionNameLabels.get(counter).setVisible(true);
                productionNameTextBoxes.get(counter).setVisible(true);
                showsInProductionLists.get(counter).setVisible(true);
                showsInProductionsLabels.get(counter).setVisible(true);
            }
            else {
                productionNameLabels.get(counter).setVisible(false);
                productionNameTextBoxes.get(counter).setVisible(false);
                showsInProductionLists.get(counter).setVisible(false);
                showsInProductionsLabels.get(counter).setVisible(false);
            }

            if (counter == 0) {
                saveButton.setVisible(true);
            }
        }
    }

    /**
     * Handles the selection of a Season to be edited
     */
    private void onSeasonSelected() {
        newSeason = false;
        create_new_season_button.setVisible(false);
        Season selectedSeason = (Season) select_season_list.getSelectedValue();

        //Ensures that the value was not changed to null
        if(selectedSeason == null) {
            return;
        }
        List<Production> productions = selectedSeason.getProductionsList();
        //get season name from backend put into sting
        season_name.setText(selectedSeason.getDisplayName());

        seasonNameLabel.setVisible(true);
        season_name.setVisible(true);

        //get productions int from backend
        int prod_num = productions.size();

        num_productions_list.setSelectedIndex(prod_num - 1);
        numberOfProductionsIntLabel.setVisible(true);
        num_productions_list.setVisible(true);

        //Nothing needed here
        num_productions_list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onNumProductionsChanged();
                Warning_num_changed.setVisible(true);
            }
        });


        //NOTHING NEEDED FOR THE LOOP
        for (int counter = 0; counter < prod_num; counter++) {
            productionNameTextBoxes.get(counter).setText(productions.get(counter).getDisplayName());
            productionNameTextBoxes.get(counter).setVisible(true);
            productionNameLabels.get(counter).setVisible(true);
            showsInProductionsLabels.get(counter).setVisible(true);
            showsInProductionLists.get(counter).setVisible(true);
            showsInProductionLists.get(counter).setSelectedIndex(productions.get(counter).getPerformances().size() - 1);

            if (counter == 0) {
                saveButton.setVisible(true);
            }
        }
    }

    /**
     * Saves the season
     */
    private void saveSeason() {
        savedLabel.setVisible(true);
        Season season;

        if (newSeason) {
            season = new Season();
        }
        else {
            season = (Season) select_season_list.getSelectedValue();
        }

        if (season == null) {
            return;
        }

        //send season name to backend
        String name = season_name.getText();
        season.setDisplayName(name);

        //Saves each production
        for (int i = 0; i < productionNameTextBoxes.size(); i++) {
            JTextArea productionTextArea = productionNameTextBoxes.get(i);
            String productionName = productionTextArea.getText();

            //If the number of productions in the list of seasons is less than i, create a new production
            if (productionTextArea.isVisible() && season.getProductionsList().size() < i + 1) {
                Production production = new Production();
                production.setDisplayName(productionName);
                int numPerformances = showsInProductionLists.get(i).getSelectedIndex() + 1;

                for (int j = 0; j < numPerformances; j++) {
                    production.addNewPerformance("New Performance");
                }

                season.addExistingProduction(production);

            }
            //Otherwise edit the existing production
            else if (productionTextArea.isVisible()) {
                Production production = season.getProductionsList().get(i);
                production.setDisplayName(productionName);

                int numPerformances = showsInProductionLists.get(i).getSelectedIndex() + 1;

                if (numPerformances > production.getPerformances().size()) {
                    for (int j = production.getPerformances().size(); j < numPerformances; j++) {
                        production.addNewPerformance("New Performance");
                    }
                }
            }
            //Delete a production that is no longer needed
            else if (season.getProductionsList().size() > i) {
                season.getProductionsList().remove(i);
            }
        }

        //Checks if this season is new or already exists
        if (newSeason) {
            controller.create_object(season.getUuid(), ObjectType.SEASON, season);
        }
        controller.save_to_memory();
        savedLabel.setVisible(true);
        populateSeasons();
    }

    /**
     * Populates the list of seasons
     */
    private void populateSeasons() {
        ArrayList<Season> seasons = (ArrayList<Season>) controller.get_root_object().getSeasonsList();

        if(seasons != null) {
            select_season_list.setListData(seasons.toArray());
        }
        else {
            return;
        }
    }


}
