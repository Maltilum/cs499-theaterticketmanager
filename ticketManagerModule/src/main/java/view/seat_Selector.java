package view;

import controller.BasicController;
import controller.FacilityName;
import model.*;
import model.ticketHolders.TicketHolder;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class seat_Selector {
    public JPanel seatSelectorlabel;

    private JLabel reservationNameLabel;
    private JTextPane reservationNameText;

    private JLabel selectSeasonLabel;
    private JList selectSeasonList;

    private JLabel selectProductionLabel;
    private JList selectProductionList;

    private JLabel selectPerformanceLabel;
    private JList selectPerformanceList;

    private JLabel selectSectionLabel;
    private JList selectSectionList;

    private JLabel selectRowLabel;
    private JList selectRowList;

    private JLabel priceOfSeatsLabel;
    private JTextArea seatpriceTextArea;

    private JLabel facilityImage;

    private JLabel seatimage1;
    private JCheckBox seatBox1;

    private JLabel seatimage2;
    private JCheckBox seatBox2;

    private JLabel seatimage3;
    private JCheckBox seatBox3;

    private JLabel seatimage4;
    private JCheckBox seatBox4;

    private JLabel seatimage5;
    private JCheckBox seatBox5;

    private JLabel seatimage6;
    private JCheckBox seatBox6;

    private JLabel seatimage7;
    private JCheckBox seatBox7;

    private JLabel seatimage8;
    private JCheckBox seatBox8;

    private JLabel seatimage9;
    private JCheckBox seatBox9;

    private JLabel seatimage10;
    private JCheckBox seatBox10;

    private JLabel seatimage11;
    private JCheckBox seatBox11;

    private JLabel seatimage12;
    private JCheckBox seatBox12;

    private JLabel seatimage13;
    private JCheckBox seatBox13;

    private JLabel seatimage14;
    private JCheckBox seatBox14;

    private JLabel seatimage15;
    private JCheckBox seatBox15;

    private JLabel seatimage16;
    private JCheckBox seatBox16;

    private JLabel seatimage17;
    private JCheckBox seatBox17;

    private JLabel seatimage18;
    private JCheckBox seatBox18;

    private JLabel paymentChoiceLabel;
    private JList paymentChoice;

    private JLabel reservationHasBeenMadeLabel;
    private JButton Confirm;
    private JLabel confirmPurchaseLabel;

    private ArrayList<JLabel> seatImages = new ArrayList<>();
    private ArrayList<JCheckBox> seatCheckBoxes = new ArrayList<>();

    private BasicController controller;

    public seat_Selector(BasicController controller) {
        this.controller = controller;

        populateSeasons();

        selectProductionLabel.setVisible(false);
        selectProductionList.setVisible(false);

        selectPerformanceLabel.setVisible(false);
        selectPerformanceList.setVisible(false);

        facilityImage.setVisible(false);

        selectSectionLabel.setVisible(false);
        selectSectionList.setVisible(false);

        selectRowLabel.setVisible(false);
        selectRowList.setVisible(false);

        priceOfSeatsLabel.setVisible(false);
        seatpriceTextArea.setVisible(false);

        paymentChoiceLabel.setVisible(false);
        paymentChoice.setVisible(false);

        confirmPurchaseLabel.setVisible(false);
        Confirm.setVisible(false);
        reservationHasBeenMadeLabel.setVisible(false);

        seatImages.add(seatimage1);
        seatImages.add(seatimage2);
        seatImages.add(seatimage3);
        seatImages.add(seatimage4);
        seatImages.add(seatimage5);
        seatImages.add(seatimage6);
        seatImages.add(seatimage7);
        seatImages.add(seatimage8);
        seatImages.add(seatimage9);
        seatImages.add(seatimage10);
        seatImages.add(seatimage11);
        seatImages.add(seatimage12);
        seatImages.add(seatimage13);
        seatImages.add(seatimage14);
        seatImages.add(seatimage15);
        seatImages.add(seatimage16);
        seatImages.add(seatimage17);
        seatImages.add(seatimage18);
        for (JLabel seatImage : seatImages) {
            seatImage.setVisible(false);
        }

        seatCheckBoxes.add(seatBox1);
        seatCheckBoxes.add(seatBox2);
        seatCheckBoxes.add(seatBox3);
        seatCheckBoxes.add(seatBox4);
        seatCheckBoxes.add(seatBox5);
        seatCheckBoxes.add(seatBox6);
        seatCheckBoxes.add(seatBox7);
        seatCheckBoxes.add(seatBox8);
        seatCheckBoxes.add(seatBox9);
        seatCheckBoxes.add(seatBox10);
        seatCheckBoxes.add(seatBox11);
        seatCheckBoxes.add(seatBox12);
        seatCheckBoxes.add(seatBox13);
        seatCheckBoxes.add(seatBox14);
        seatCheckBoxes.add(seatBox15);
        seatCheckBoxes.add(seatBox16);
        seatCheckBoxes.add(seatBox17);
        seatCheckBoxes.add(seatBox18);
        for (JCheckBox seatCheckBox : seatCheckBoxes) {
            seatCheckBox.setVisible(false);
            seatCheckBox.setSelected(false);
        }


        //Send the selected season to backend to get the production list
        selectSeasonList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onSeasonSelected();
            }
        });

        //Send the selected production to backend to get the show list
        selectProductionList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onProductionSelected();
            }
        });

        //send the selected show to backend to get the section list
        //also receive facility information
        selectPerformanceList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onPerformanceSelected();
            }
        });

        //send the selected section to backend to get the row list
        //also get the facility information in an int, or a string TBD
        selectSectionList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onSectionSelected();
            }
        });

        //send the row information to backend to get the price, seats in the row, and status of seat
        selectRowList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onRowSelected();
            }
        });

        //Nothing needed here
        paymentChoice.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                confirmPurchaseLabel.setVisible(true);
                Confirm.setVisible(true);
            }
        });

        //Send reservation name, season, production, show, section, row, seat status, and payment type
        Confirm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onConfirm();
            }
        });


    }


    /**
     * Populates the list of seasons
     */
    private void populateSeasons() {
        ArrayList<Season> seasons = (ArrayList<Season>) controller.get_root_object().getSeasonsList();

        if (seasons != null) {
            selectSeasonList.setListData(seasons.toArray());
        }
        else {
            return;
        }
    }

    /**
     * Handles a Season being selected from the list
     */
    private void onSeasonSelected() {
        Season season = (Season) selectSeasonList.getSelectedValue();
        //send season list to backend and populate the production list

        if (season == null) {
            selectProductionList.setVisible(false);
            selectProductionLabel.setVisible(false);
            return;
        }

        //get production list from backend
        ArrayList<Production> productions = (ArrayList<Production>) season.getProductionsList();
        selectProductionList.setListData(productions.toArray());

        selectProductionList.setVisible(true);
        selectProductionLabel.setVisible(true);

        selectPerformanceLabel.setVisible(false);
        selectPerformanceList.setVisible(false);
        facilityImage.setVisible(false);
        selectSectionLabel.setVisible(false);
        selectSectionList.setVisible(false);
        selectRowLabel.setVisible(false);
        selectRowList.setVisible(false);
        priceOfSeatsLabel.setVisible(false);
        seatpriceTextArea.setVisible(false);
        paymentChoiceLabel.setVisible(false);
        paymentChoice.setVisible(false);
        confirmPurchaseLabel.setVisible(false);
        Confirm.setVisible(false);
        reservationHasBeenMadeLabel.setVisible(false);

        for (JLabel seatImage : seatImages) {
            seatImage.setVisible(false);
        }

        for (JCheckBox seatCheckBox : seatCheckBoxes) {
            seatCheckBox.setVisible(false);
            seatCheckBox.setSelected(false);
        }
    }

    /**
     * Handles a production being selected from the list
     */
    private void onProductionSelected() {
        Production production = (Production) selectProductionList.getSelectedValue();

        if (production == null) {
            selectPerformanceLabel.setVisible(false);
            selectPerformanceList.setVisible(false);
            return;
        }

        ArrayList<Performance> performances = production.getPerformances();
        //get show list from backend
        selectPerformanceList.setListData(performances.toArray());

        selectPerformanceList.setVisible(true);
        selectPerformanceLabel.setVisible(true);

        facilityImage.setVisible(false);
        selectSectionLabel.setVisible(false);
        selectSectionList.setVisible(false);
        selectRowLabel.setVisible(false);
        selectRowList.setVisible(false);
        priceOfSeatsLabel.setVisible(false);
        seatpriceTextArea.setVisible(false);
        paymentChoiceLabel.setVisible(false);
        paymentChoice.setVisible(false);
        confirmPurchaseLabel.setVisible(false);
        Confirm.setVisible(false);
        reservationHasBeenMadeLabel.setVisible(false);

        for (JLabel seatImage : seatImages) {
            seatImage.setVisible(false);
        }

        for (JCheckBox seatCheckBox : seatCheckBoxes) {
            seatCheckBox.setVisible(false);
            seatCheckBox.setSelected(false);
        }
    }

    /**
     * Handles a performance being selected from the list
     */
    private void onPerformanceSelected() {
        Performance performance = (Performance) selectPerformanceList.getSelectedValue();

        if (performance == null) {
            selectSectionList.setVisible(false);
            selectSectionLabel.setVisible(false);
            return;
        }

        FacilityName facilityName = performance.getFacilityName();

        if (facilityName == null || facilityName.equals(FacilityName.EMPTY)) {
            selectSectionList.setVisible(false);
            selectSectionLabel.setVisible(false);
            facilityImage.setVisible(false);
            return;
        }
        else if (facilityName.equals(FacilityName.PLAYHOUSE)) {
            facilityImage.setVisible(true);
            facilityImage.setIcon(new ImageIcon(getClass().getResource("/PlayhouseStage.png")));
            //set to playhouse image
        }
        else if (facilityName.equals(FacilityName.CONCERT_HALL)) {
            facilityImage.setVisible(true);
            facilityImage.setIcon(new ImageIcon(getClass().getResource("/ConcertHall.png")));
            //set to concert hall image
        }

        selectSectionList.setListData(performance.getFacility().getSections().toArray());

        selectSectionList.setVisible(true);
        selectSectionLabel.setVisible(true);
        facilityImage.setVisible(true);

        selectRowLabel.setVisible(false);
        selectRowList.setVisible(false);
        priceOfSeatsLabel.setVisible(false);
        seatpriceTextArea.setVisible(false);
        paymentChoiceLabel.setVisible(false);
        paymentChoice.setVisible(false);
        confirmPurchaseLabel.setVisible(false);
        Confirm.setVisible(false);
        reservationHasBeenMadeLabel.setVisible(false);

        for (JLabel seatImage : seatImages) {
            seatImage.setVisible(false);
        }

        for (JCheckBox seatCheckBox : seatCheckBoxes) {
            seatCheckBox.setVisible(false);
            seatCheckBox.setSelected(false);
        }
    }

    /**
     * Handles a Section being selected from the list
     */
    private void onSectionSelected() {
        Section section = (Section) selectSectionList.getSelectedValue();

        if (section == null) {
            selectRowLabel.setVisible(false);
            selectRowList.setVisible(false);
            return;
        }

        //get row list from backend
        selectRowList.setListData(section.getRows().toArray());

        facilityImage.setVisible(true);
        selectRowList.setVisible(true);
        selectRowLabel.setVisible(true);

        priceOfSeatsLabel.setVisible(false);
        seatpriceTextArea.setVisible(false);
        paymentChoiceLabel.setVisible(false);
        paymentChoice.setVisible(false);
        confirmPurchaseLabel.setVisible(false);
        Confirm.setVisible(false);
        reservationHasBeenMadeLabel.setVisible(false);

        for (JLabel seatImage : seatImages) {
            seatImage.setVisible(false);
        }

        for (JCheckBox seatCheckBox : seatCheckBoxes) {
            seatCheckBox.setVisible(false);
            seatCheckBox.setSelected(false);
        }
    }

    private void onRowSelected() {
        Row row = (Row) selectRowList.getSelectedValue();

        if (row == null) {
            paymentChoiceLabel.setVisible(false);
            paymentChoice.setVisible(false);

            for (JLabel seatImage : seatImages) {
                seatImage.setVisible(false);
            }

            for (JCheckBox seatCheckBox : seatCheckBoxes) {
                seatCheckBox.setVisible(false);
                seatCheckBox.setSelected(false);
            }
            return;
        }

        ArrayList<Seat> seats = row.getSeats();
        //Populate the seats that are in the row
        for (int counter = 0; counter < seatImages.size(); counter++) {
            if (counter >= seats.size()) {
                seatImages.get(counter).setVisible(false);
                seatCheckBoxes.get(counter).setVisible(false);
                continue;
            }
            Seat seat = seats.get(counter);

            //Coordinate the image and checkbox with the status of the seat
            if (seat.getOccupant() != null) {
                //Set seat image to taken, disable checkbox
                seatImages.get(counter).setIcon(new ImageIcon(getClass().getResource("/TakenSeat.png")));
                seatCheckBoxes.get(counter).setEnabled(false);
            }
            else if (seat.isAccessible()) {
                //set seat image to handi
                seatImages.get(counter).setIcon(new ImageIcon(getClass().getResource("/HandiSeat.png")));
                seatCheckBoxes.get(counter).setEnabled(true);
            }
            else {
                //set seat image to free
                seatImages.get(counter).setIcon(new ImageIcon(getClass().getResource("/OpenSeat.png")));
                seatCheckBoxes.get(counter).setEnabled(true);
            }
            seatImages.get(counter).setVisible(true);
            seatCheckBoxes.get(counter).setVisible(true);
        }

        paymentChoiceLabel.setVisible(true);
        paymentChoice.setVisible(true);
        selectRowList.setVisible(true);
        selectRowLabel.setVisible(true);
        seatpriceTextArea.setText(Float.toString(row.getPrice()));
        seatpriceTextArea.setVisible(true);
        Confirm.setVisible(false);
    }

    /**
     * Process ticket purchase confirmation
     */
    private void onConfirm() {
        int numSeats = 0;

        Row row = (Row) selectRowList.getSelectedValue();

        if (row == null) {
            return;
        }

        ArrayList<Seat> seats = row.getSeats();

        String name = reservationNameText.getText();
        if (name == null || name.isBlank()) {
            reservationHasBeenMadeLabel.setText("Invalid Reservation Name");
            reservationHasBeenMadeLabel.setVisible(true);
            return;
        }

        //Get a list of selected seats
        ArrayList<Seat> selectedSeats = new ArrayList<>();
        for (int counter = 0; counter < seats.size(); counter++) {
            JCheckBox checkBox = seatCheckBoxes.get(counter);
            //Determine whether this seat is selected
            if (checkBox.isEnabled() && checkBox.isSelected()) {
                selectedSeats.add(seats.get(counter));
            }
        }

        //Return if no seats are selected
        if (selectedSeats.size() == 0) {
            reservationHasBeenMadeLabel.setText("You must select a seat to continue");
            reservationHasBeenMadeLabel.setVisible(true);
            return;
        }

        for (Seat seat : selectedSeats) {
            addTicketHolderToSeat(seat, name);
        }
        controller.save_to_memory();
        reservationHasBeenMadeLabel.setText("Reservation has been successfully made!");
        reservationHasBeenMadeLabel.setVisible(true);
    }

    /**
     * Adds a TicketHolder to the seat
     *
     * @param seat The seat to be purchased
     * @param name The name of the TicketHolder
     */
    private void addTicketHolderToSeat(Seat seat, String name) {
        TicketHolder ticketHolder = new TicketHolder();
        ticketHolder.setDisplayName(name);
        ticketHolder.setDisabled(seat.isAccessible());
        ticketHolder.setPaid(paymentChoice.getSelectedIndex() == 3);
        ticketHolder.setTicketPrice(Float.parseFloat(seatpriceTextArea.getText()));
        seat.setOccupant(ticketHolder, false);
        controller.save_to_memory();
    }
}
