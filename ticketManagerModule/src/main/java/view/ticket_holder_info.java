package view;

import controller.BasicController;
import controller.FacilityName;
import controller.ObjectType;
import model.Facility;
import model.Row;
import model.Seat;
import model.Section;
import model.ticketHolders.PostalAddress;
import model.ticketHolders.SeasonTicketHolder;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ticket_holder_info {
    public JPanel ticket_holder_info;

    private JTextField fullNameTextField;
    private JTextField addressLine1TextField;
    private JTextField addressLine2TextField;
    private JTextField cityTextField;
    private JTextField stateTextField;
    private JTextField zipCodeTextField;
    private JTextField phoneNumberTextField;

    private JCheckBox checkIfAccomidationsNeededCheckBox;
    private JCheckBox checkBoxIfTicketCheckBox;

    private JList sectionList;
    private JList passholderNames;
    private JList paymentStatus;
    private JList seatList;
    private JList rowList;

    private JLabel needsDisabiltyAccomidationsLabel;
    private JLabel fullNameLabel;
    private JLabel addressLine1Label;
    private JLabel addressLine2Label;
    private JLabel cityLabel;
    private JLabel stateLabel;
    private JLabel zipCodeLabel;
    private JLabel phoneNumberLabel;
    private JLabel assignedSectionLabel;
    private JLabel rowLabel;
    private JLabel seatLabel;

    private JButton retrieveDataFromSelectedButton;
    private JButton saveButton;
    private JList facilitychoice;
    private JLabel facilityChoiceText;

    private JLabel current_prefs_label;
    private JTextField section_pref;
    private JTextField row_pref;
    private JTextField seat_pref;

    private JCheckBox seat_change;
    private JLabel savedfeedback;
    private BasicController controller;

    private SeasonTicketHolder seasonTicketHolder;
    private boolean createNewSth = true;


    /**
     * Loads and handles the view to create and edit season ticket holders
     *
     * @param controller The controller for the view
     */
    public ticket_holder_info(BasicController controller) {
        this.controller = controller;

        //get the passholder names in a list from backend
        populateSthList();

        //facilitychoice

        assignedSectionLabel.setVisible(false);
        sectionList.setVisible(false);
        rowLabel.setVisible(false);
        rowList.setVisible(false);
        seatLabel.setVisible(false);
        seatList.setVisible(false);

        current_prefs_label.setVisible(false);
        section_pref.setVisible(false);
        row_pref.setVisible(false);
        seat_pref.setVisible(false);
        seat_change.setVisible(false);
        facilitychoice.setListData(FacilityName.values());
        savedfeedback.setVisible(false);

        //Send selected name
        //Get Name, address, city, state, zip, phone, payment status, and disibility status
        //Section, Row, and Seat
        retrieveDataFromSelectedButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                loadTicketHolder();
            }
        });

        //send facility to get section
        facilitychoice.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onFacilitySelected();
            }
        });

        sectionList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onSectionSelected();
            }
        });

        //send section to get row
        rowList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onRowSelected();
            }
        });

        //send row to get seat
        seatList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                onSeatSelected();
            }
        });


        //Send name, address, city, state, zip, phone, disibilty seat needs,
        // pref section, pref row, pref seat, and payment status.
        saveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onSave();
            }
        });

    }

    /**
     * Handles a facility being selected
     */
    private void onFacilitySelected() {
        FacilityName facilityName = (FacilityName) facilitychoice.getSelectedValue();
        Facility facility;

        facility = getFacilityFromFacilityName(facilityName);

        if (facilityName == null) {
            sectionList.setVisible(false);
            assignedSectionLabel.setVisible(false);
            return;
        }

        sectionList.setListData(facility.getSections().toArray());

        assignedSectionLabel.setVisible(true);
        sectionList.setVisible(true);
        rowLabel.setVisible(false);
        rowList.setVisible(false);
        seatLabel.setVisible(false);
        seatList.setVisible(false);
    }

    /**
     * Handles a section being selected
     */
    private void onSectionSelected() {
        Section section = (Section) sectionList.getSelectedValue();

        if (section == null) {
            rowList.setVisible(false);
            rowList.setVisible(false);
            return;
        }

        rowList.setListData(section.getRows().toArray());

        rowLabel.setVisible(true);
        rowList.setVisible(true);
        seatLabel.setVisible(false);
        seatList.setVisible(false);
    }

    /**
     * Handles a row being selected
     */
    private void onRowSelected() {
        Row row = (Row) rowList.getSelectedValue();

        if (row == null) {
            seatList.setVisible(false);
            seatLabel.setVisible(false);
            return;
        }

        seatList.setListData(row.getSeats().toArray());
        seatLabel.setVisible(true);
        seatList.setVisible(true);
    }

    /**
     * Handles a seat being selected
     */
    private void onSeatSelected() {
        Seat seat = (Seat) seatList.getSelectedValue();
        if (seat == null) {
            seat_change.setVisible(false);
            return;
        }

        seat_change.setVisible(true);
    }

    /**
     * Loads the selected SeasonTicketHolder
     */
    private void loadTicketHolder() {
        seasonTicketHolder = (SeasonTicketHolder) passholderNames.getSelectedValue();
        if (seasonTicketHolder == null) {
            return;
        }

        createNewSth = false;


        //Set selected name
        fullNameTextField.setText(seasonTicketHolder.getDisplayName());
        PostalAddress ticketHolderAddress = seasonTicketHolder.getAddress();

        if (ticketHolderAddress != null) {
            addressLine1TextField.setText(ticketHolderAddress.getStreet());
            addressLine2TextField.setText(ticketHolderAddress.getLine2());
            cityTextField.setText(ticketHolderAddress.getCity());
            stateTextField.setText(ticketHolderAddress.getRegion());
            zipCodeTextField.setText(ticketHolderAddress.getZipCode());
        }

        phoneNumberTextField.setText(seasonTicketHolder.getPhone_number());

        checkBoxIfTicketCheckBox.setSelected(seasonTicketHolder.getPaid());

        checkIfAccomidationsNeededCheckBox.setSelected(seasonTicketHolder.getDisabled());

        loadSeatingPreferences(seasonTicketHolder.getSeating_preferences());

        current_prefs_label.setVisible(true);
        section_pref.setVisible(true);
        row_pref.setVisible(true);
        seat_pref.setVisible(true);

        assignedSectionLabel.setVisible(true);
        sectionList.setVisible(true);
        rowLabel.setVisible(true);
        rowList.setVisible(true);
        seatLabel.setVisible(true);
        seatList.setVisible(true);
        seat_change.setVisible(true);
    }

    /**
     * Selects the seating preferences of the Season Ticket Holder that is being loaded
     *
     * @param preferences
     */
    private void loadSeatingPreferences(List<String> preferences) {
        if(preferences == null) {
            return;
        }

        String facilityPref = preferences.get(0);
        String sectionPref = preferences.get(1);
        String rowPref = preferences.get(2);
        String seatPref = preferences.get(3);

        if(preferences.size() < 1) {
            return;
        }

        for (int i = 0; i < facilitychoice.getModel().getSize(); i++) {
            FacilityName currentName = (FacilityName) facilitychoice.getModel().getElementAt(i);
            if (facilityPref.equals(currentName.label)) {
                facilitychoice.setSelectedIndex(i);
            }
        }

        if(preferences.size() < 2) {
            return;
        }

        for (int i = 0; i < sectionList.getModel().getSize(); i++) {
            Section currentSection = (Section) sectionList.getModel().getElementAt(i);
            if (currentSection.getDisplayName().equals(sectionPref)) {
                sectionList.setSelectedIndex(i);
            }
        }

        if(preferences.size() < 3) {
            return;
        }

        for (int i = 0; i < rowList.getModel().getSize(); i++) {
            Row currentRow = (Row) rowList.getModel().getElementAt(i);
            if (currentRow.getDisplayName().equals(rowPref)) {
                rowList.setSelectedIndex(i);
            }
        }

        if(preferences.size() < 4) {
            return;
        }

        for (int i = 0; i < seatList.getModel().getSize(); i++) {
            Seat currentSeat = (Seat) seatList.getModel().getElementAt(i);
            if (currentSeat.getDisplayName().equals(seatPref)) {
                seatList.setSelectedIndex(i);
            }
        }
    }

    /**
     * Populates the list of existing Season Ticket Holders
     */
    private void populateSthList() {
        ArrayList<SeasonTicketHolder> ticketHolders = (ArrayList<SeasonTicketHolder>) controller.get_root_object().getSeasonTicketHolders();

        if (ticketHolders != null) {
            passholderNames.setListData(ticketHolders.toArray());
        }
    }

    /**
     * Handles saving a season ticket holder
     */
    private void onSave() {
        if (createNewSth) {
            seasonTicketHolder = new SeasonTicketHolder();
        }

        //Get name
        String name = fullNameTextField.getText();
        if (name == null || name.isBlank()) {
            savedfeedback.setText("Invalid Name");
            savedfeedback.setVisible(true);
            return;
        }
        seasonTicketHolder.setDisplayName(fullNameTextField.getText());

        //Get address
        PostalAddress postalAddress = getPostalAddressInfo();
        if (postalAddress == null) {
            savedfeedback.setText("Invalid Postal Address");
            savedfeedback.setVisible(true);
            return;
        }
        seasonTicketHolder.setAddress(postalAddress);

        //Get phone number
        String phoneNumber = phoneNumberTextField.getText();
        if (phoneNumber == null) {
            savedfeedback.setText("Invalid Phone Number");
            savedfeedback.setVisible(true);
            return;
        }
        seasonTicketHolder.setPhone_number(phoneNumber);

        //Get seating preferences
        List<String> seatingPreferences = getSeatingPreferences();
        if (seatingPreferences == null) {
            savedfeedback.setText("Invalid Seating Preferences");
            savedfeedback.setVisible(true);
            return;
        }
        seasonTicketHolder.setSeating_preferences(seatingPreferences);

        //Get whether accomodations are needed
        seasonTicketHolder.setDisabled(checkIfAccomidationsNeededCheckBox.isSelected());

        //Get whether the ticketholder has paid
        seasonTicketHolder.setPaid(checkBoxIfTicketCheckBox.isSelected());

        if (createNewSth) {
            controller.create_object(seasonTicketHolder.getUuid(), ObjectType.S_TICKET_HOLDER, seasonTicketHolder);
        }
        controller.save_to_memory();
        savedfeedback.setText("Saved Successfully");
        savedfeedback.setVisible(true);
    }

    /**
     * Creates an address based on the information in the view
     *
     * @return The new PostalAddress
     */
    private PostalAddress getPostalAddressInfo() {
        PostalAddress postalAddress = new PostalAddress();
        String addressLine1 = addressLine1TextField.getText();
        String addressLine2 = addressLine2TextField.getText();
        String city = cityTextField.getText();
        String state = stateTextField.getText();
        String zipCode = zipCodeTextField.getText();

        if (addressLine1 == null || city == null || state == null || zipCode == null || addressLine1.isBlank() ||
                city.isBlank() || state.isBlank() || zipCode.isBlank()) {
            return null;
        }

        if (addressLine2 == null || addressLine2.isBlank()) {
            addressLine2 = "";
        }

        postalAddress.setStreet(addressLine1);
        postalAddress.setLine2(addressLine2);
        postalAddress.setCity(city);
        postalAddress.setRegion(state);
        postalAddress.setZipCode(zipCode);

        return postalAddress;
    }

    /**
     * Processes the seating preferences of the SeasonTicketHolder
     *
     * @return A list of seating preferences
     */
    private List<String> getSeatingPreferences() {
        ArrayList<String> preferences = new ArrayList<>();
        FacilityName facilityName = (FacilityName) facilitychoice.getSelectedValue();
        Section sectionPref = (Section) sectionList.getSelectedValue();
        Row rowPref = (Row) rowList.getSelectedValue();
        Seat seatPref = (Seat) seatList.getSelectedValue();

        Facility facilityPref = getFacilityFromFacilityName(facilityName);

        if (facilityPref == null || sectionPref == null || rowPref == null || seatPref == null) {
            return null;
        }
        preferences.add(facilityPref.getDisplayName());
        preferences.add(sectionPref.getDisplayName());
        preferences.add(rowPref.getDisplayName());
        preferences.add(seatPref.getDisplayName());

        return preferences;
    }

    /**
     * Returns a facility associated with a FacilityName
     *
     * @param facilityName The FacilityName Enum
     * @return The associated Facility
     */
    private Facility getFacilityFromFacilityName(FacilityName facilityName) {
        Facility facility;
        if (facilityName == null || facilityName.equals(FacilityName.EMPTY)) {
            return null;
        }
        else if (facilityName.equals(FacilityName.PLAYHOUSE)) {
            facility = controller.get_root_object().loadPlayHouse();
        }
        else {
            facility = controller.get_root_object().loadConcertHall();
        }

        return facility;
    }
}

