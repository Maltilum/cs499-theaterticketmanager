package view.xmlUtils;

import com.sun.xml.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "response")
public class STicketDataHolder extends XmlDataHolder {
    public String uuid;
    public String ticketPrice;
    public boolean paid;
    public boolean disabled;
    public String miscNotes;
    public String phone_number;
}
