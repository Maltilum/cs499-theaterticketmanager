package view.xmlUtils;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * Abstract class that is meant to hold data form XML strings.
 *
 * To unmarshal XML string, create a child of this abstract class and call XmlUtils.unmarshal(string, Class.class);
 *
 * It's also important to ALWAYS make the name field of @XmlRootElement equal to what you expact the root element of the xml string to be.
 */
@XmlRootElement(name = "response")
public abstract class XmlDataHolder {

/**
 * In instances of this class, you should create public varibles that have names matching the names of the XML fields you want to extract.
 *
 * for instance if you wanted to get the data out of an xml string in the form of
 * <response>
 *     <uuid>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</uuid>
 *     <ticketPrice>42.42</ticketPrice>
 *     <paid>false</paid>
 * </response>
 *
 * You would add the varibles below.
 * public String uuid;
 * public Float/String (either would work) TicketPrice
 * public boolean false
 *
 * --------------------------------------------------------------------------
 *
 * Having extra fields in your class or your xml string won't cause any issues on its own.
 *
 * If there are fields in the class that are not in the XML, they'll just be null.
 *
 * If there are extra fields in the XML that are not in the class, that data will just not be converted.
 *
 * --------------------------------------------------------------------------------
 *
 * If for whatever reason a varible needs to have a different name from it's xml field
 *  Say the xml field name is reserved in java, then you can use the annotation @XmlField(name="the name of the xml field") to make that work.
 *
 *  For instance.
 *
 * @XmlElement(name="class")
 *  public String xmlClassField
 */





}
