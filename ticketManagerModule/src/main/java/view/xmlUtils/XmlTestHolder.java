package view.xmlUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "response")
public class XmlTestHolder extends XmlDataHolder {

    @XmlElement
    public String testString;

}
