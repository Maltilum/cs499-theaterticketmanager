package view.xmlUtils;

import javax.xml.bind.JAXB;
import java.io.StringReader;

public class XmlUtils {


    /**
     * @param xml Xml string that you want to unmarshal.
     * @param clazz The class context you want to unmarshal to.
     * @param <T> Any class that can be unmarshaled.
     * @return An instance of the class after being unmarshaled, needs to be cast back into what your doing.
     */
    public  static <T extends XmlDataHolder> T unmarshall(String xml, Class<T> clazz){
        return clazz.cast(JAXB.unmarshal(new StringReader(xml), clazz));
    }



}
