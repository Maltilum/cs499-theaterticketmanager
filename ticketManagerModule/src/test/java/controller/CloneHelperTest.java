package controller;

import model.ticketHolders.TicketHolder;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBException;

import static org.junit.jupiter.api.Assertions.*;

class CloneHelperTest {

    @Test
    public void DeepCloneTest(){
        TicketHolder th = new TicketHolder();
        th.setDisplayName("Billy Bob");

        try {
            TicketHolder clone = (TicketHolder) CloneHelper.deepClone(th);
            clone.setDisplayName("Tommy Tompson");

            Assert.assertNotEquals(clone.getDisplayName(), th.getDisplayName());
        } catch (JAXBException e) {
            Assert.fail(e.toString());
        }
    }

}