package controller;

import model.Production;
import model.Root;
import model.Season;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

class basicControllerTest {


    @Test
    public void get_object_dataTest(){
        Root root = new Root();
        // the correct way to get today's date
        Date today = Calendar.getInstance().getTime();
        Season testAddingSeason = new Season();
        testAddingSeason.setDisplayName("Season that was added artifiacally");
        root.createNewSeason("TestSeason", today, today);
        root.createNewSeason("She'll be comin round the mountain", today, today);
        root.addExistingSeason(testAddingSeason);
        BasicController basicController = new BasicController(root);

        Season testFindSeason = (Season) basicController.get_object_data(root.getSeasonsList().get(0).getUuid());
        testFindSeason.setDisplayName("Changed it later");
        Assert.assertNotEquals("TestSeason", testFindSeason.getDisplayName() );

        testFindSeason = (Season) basicController.get_object_data(root.getSeasonsList().get(2).getUuid());

        Assert.assertEquals(testAddingSeason.getDisplayName(), testFindSeason.getDisplayName());

        testFindSeason.setDisplayName("Display Name is now changed");

        Assert.assertNotEquals(testAddingSeason.getDisplayName(), testFindSeason.getDisplayName());
    }

    @Test
    public void get_object_ref_TEST(){
        Root root = new Root();
        Season testAddingSeason = new Season();
        testAddingSeason.setDisplayName("Season that was added artifiacally");
        root.addExistingSeason(testAddingSeason);
        BasicController basicController = new BasicController(root);

        Season testGetRef = (Season)basicController.get_object_ref(testAddingSeason.getUuid());

        testGetRef.setDisplayName("Should both be the same");

        Assert.assertEquals(testGetRef.getDisplayName(), root.getSeasonsList().get(0).getDisplayName());
    }

    @Test
    public void create_object_TEST(){
        Root root = new Root();
        Season testAddingSeason = new Season();
        testAddingSeason.setDisplayName("Season that was added artifiacally");
        root.addExistingSeason(testAddingSeason);
        BasicController basicController = new BasicController(root);

        Production testCreateData = new Production();
        testCreateData.setDisplayName("The day");
        Production result = (Production)basicController.create_object(testAddingSeason.getUuid(), ObjectType.PRODUCTION, testCreateData);

        Assert.assertEquals(result.getDisplayName(), testCreateData.getDisplayName());


    }

}