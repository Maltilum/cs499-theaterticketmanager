package core;

import model.*;
import model.ticketHolders.TicketHolder;
import presenters.BasicPresenter;


import java.util.Scanner;

public class commandLineTest {

    Scanner scanner = new Scanner(System.in);

    public static void main (String[] args) {
        //Program loops until this is true.
        boolean stopLoop = false;
        Root root = new Root();

        //Gets command line input, waits for input before continuing.
        Scanner scanner = new Scanner(System.in);

        //PresenterCore functionality of the program.
        try {
            BasicPresenter presenterCore = new BasicPresenter();
            while (!stopLoop) {
                //Just so something is printed to the command line to show that it's working.
                System.out.println("Input Command:");

                //Get next line, aka wait for the user to put something in the terminal.
                String command = scanner.next();
                String[] arguments = command.split("-");

                //Switch based on the command given.
                switch(arguments[0].replace(" ", "")){
                    case "q":
                        stopLoop = true;
                        break;
                    case "test":
                        break;
                    case "add":
                        switch(arguments[1]){
                            case "th":
                            break;
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



}
