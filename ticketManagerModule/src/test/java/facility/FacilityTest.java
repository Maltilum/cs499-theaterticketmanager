package facility;

import model.Facility;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import model.Section;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Test for the {@link Facility} class
 */
class FacilityTest {
    private final String THEATER_NAME = "Theater";
    private final int CAPACITY = 1000;
    private Facility uut;

    @BeforeEach
    public void setUp() {
        uut = createUut();
    }

    /**
     * Tests that the getter for the facility name works properly
     */
    @Test
    public void getFacilityName() {
        Assert.assertEquals("Facility name does not match constant: " + uut.getDisplayName(), THEATER_NAME, uut.getDisplayName());
    }

    /**
     * Tests that the setter method for the facility name works properly
     */
    @Test
    public void setFacilityName() {
        String newName = "new name";
        uut.setDisplayName(newName);
        Assert.assertEquals("Facility name did not match the newly set name: " + uut.getDisplayName(), newName, uut.getDisplayName());
    }

    /**
     * Tests that the getCapacity method works properly
     */
    @Test
    public void getCapacity() {
        Assert.assertEquals("Facility did not have expected capacity: " + uut.getCapacity(), CAPACITY, uut.getCapacity());
    }

    /**
     * Tests that the setCapacity method works properly
     */
    @Test
    public void setCapacity() {
        int newCapacity = 100;
        uut.setCapacity(100);
        Assert.assertEquals("Facility did not have the newly set capacity: " + uut.getCapacity(), newCapacity, uut.getCapacity());
    }

    /**
     * Tests the getter and setter methods for the Sections ArrayList of a Facility
     */
    @Test
    public void testSetAndSetSections() {
        ArrayList<Section> sections = new ArrayList<Section>();
        sections.add(new Section());
        uut.setSections(sections);
        Assert.assertEquals("Facility did not have the expected list of sections", sections, uut.getSections());
    }

    /**
     * Tests adding and getting a section for a Facility
     */
    @Test
    public void testAddAndGetSection() {
        Section section = new Section();
        UUID uuid = section.getUuid();

        uut.addExistingSection(section);

        Section newSection = uut.getSection(uuid);

        Assert.assertEquals("Section not added properly", section, newSection);
    }

    /**
     * Tests removing a section from a Facility
     */
    @Test
    public void removeSection() {
        Section section = new Section();
        UUID uuid = section.getUuid();

        uut.addExistingSection(section);

        Assert.assertTrue("Section not removed properly", uut.removeSection(uuid));
        Assert.assertNull("Section  was found after being removed", uut.getSection(uuid));
    }

    /**
     * Tests that the getUuid method works properly
     */
    @Test
    public void getUuid() {
        Assert.assertNotNull("Facility did not have UUID", uut.getUuid());
    }

    /**
     * Generates a unit under test.
     *
     * @return The Facility to be tested.
     */
    private Facility createUut() {
        return new Facility(THEATER_NAME, CAPACITY, new ArrayList<Section>());
    }

    /**
     * Test to make sure the equals method is functioning properly
     */
    @Test
    void testEquals() {
        Facility copy = uut;
        Assert.assertTrue("Overridden equals method returned false when true was expected", uut.equals(copy));
        copy.setDisplayName("Changed");
        Assert.assertTrue("Overridden equals method returned true when false was expected", uut.equals(copy));
    }
}