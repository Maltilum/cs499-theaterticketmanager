package model;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Calendar;
import java.util.Date;

/**
 * Test class for a {@link Performance} object
 */
class PerformanceTest {
    private final String TITLE = "Title";
    private final Date DATE_AND_TIME = new Date(2020, Calendar.MAY, 6, 14, 0);
    private final boolean IS_CANCELLED = false;
    private final Facility FACILITY = new Facility();

    private Performance uut;

    @BeforeEach
    public void setUp() {
        uut = getUut(TITLE, DATE_AND_TIME, IS_CANCELLED, FACILITY);
    }

    /**
     * Tests whether the getUuid functions as expected
     */
    @Test
    public void getUuid() {
        Assert.assertNotNull("getUuid returned null", uut.getUuid());
    }

    /**
     * Tests whether the getTitle method functions properly
     */
    @Test
    public void getTitle() {
        String title = uut.getDisplayName();
        Assert.assertEquals("getTitle did not return the expected String: " + title, TITLE, title);
    }

    /**
     * Tests whether the setTitle method properly updates the title of the Performance
     */
    @Test
    public void setTitle() {
        String title = TITLE + "new";
        uut.setDisplayName(title);
        String newTitle = uut.getDisplayName();
        Assert.assertEquals("setTitle did not set the title to the expected value: " + newTitle, title, newTitle);
    }

    /**
     * Tests whether the getDateAndTime method returns the expected time
     */
    @Test
    public void getDateAndTime() {
        Date dateAndTime = uut.getDateAndTime();
        Assert.assertEquals("Time received by getDateAndTime was not expected: " + dateAndTime, DATE_AND_TIME, dateAndTime);
    }

    /**
     * Tests whether the setDateAndTime method functions properly
     */
    @Test
    public void setDateAndTime() {
        Date date = new Date(2020);
        uut.setDateAndTime(date);
        Date newDate = uut.getDateAndTime();
        Assert.assertEquals("Date was not properly updated by setDateAndTime: " + newDate.toString(), date, newDate);
    }

    /**
     * Tests whether the isCancelled method returns the expected value
     */
    @Test
    public void isCancelled() {
        boolean cancelled = uut.isCancelled();
        Assert.assertEquals("isCancelled returned a different value than expected: " + cancelled, IS_CANCELLED, cancelled);
    }

    /**
     * Tests whether setCancelled properly updates the Performance
     */
    @Test
    public void setCancelled() {
        boolean cancelled = !IS_CANCELLED;
        uut.setCancelled(cancelled);
        boolean newCancelled = uut.isCancelled();
        Assert.assertEquals("setCancelled did not properly update the value of cancelled: " + newCancelled, cancelled, newCancelled);
    }

    /**
     * Tests whether the getFacility method functions properly
     */
    @Test
    public void getFacility() {
        Facility facility = uut.getFacility();
        Assert.assertEquals("getFacility did not return the expected method: " + facility, FACILITY, facility);
    }

    /**
     * Tests whether setFacility properly updates the Performance
     */
    @Test
    public void setFacility() {
        Facility facility = new Facility();
        facility.setDisplayName("this one");
        uut.setFacility(facility);
        Facility newFacility = uut.getFacility();
        Assert.assertEquals("setFacility did not properly update the facility: " + newFacility, facility, newFacility);
    }

    /**
     * Gets a unit under test with the specified values
     *
     * @param title       The title of the model.performance
     * @param dateAndTime The date and time at which the model.performance will occur
     * @param isCancelled Whether the model.performance has been cancelled
     * @param facility    The facility at which the model.performance will take place
     * @return The UUT with the specified values
     */
    private Performance getUut(String title, Date dateAndTime, boolean isCancelled, Facility facility) {
        return new Performance(title, dateAndTime, isCancelled, facility);
    }
}