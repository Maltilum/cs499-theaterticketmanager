package model.dataHelpers;

import model.Root;
import model.ticketHolders.PostalAddress;
import model.ticketHolders.SeasonTicketHolder;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CSV_FactoryTest {

    //@Test
    public void ExportListTest(){
        Root root = new Root();
        List<SeasonTicketHolder> stList =  root.getSeasonTicketHolders();

        SeasonTicketHolder bob = new SeasonTicketHolder();
        bob.setDisplayName("bob");
        bob.setAddress(new PostalAddress("11111 Java Str", "", "Program Town", "Al", "00000"));
        stList.add(bob);

        SeasonTicketHolder sam = new SeasonTicketHolder();
        sam.setDisplayName("sam");
        sam.setAddress(new PostalAddress("22222 Java Str", "Apt 12", "Program Town", "Al", "111111"));
        stList.add(sam);

        CSV_Factory factory = new CSV_Factory();

        try {
            factory.exportAddressList(stList, "./data/test.csv");
        } catch (IOException e) {

            Assert.fail(e.toString());
        }


    }


    public void ImportCsvTest(){

        CSV_Factory factory = new CSV_Factory();

//        try {
//            HashMap<String, PostalAddress> result = factory.importAddressListBasic("./data/test.csv");
//            for(Map.Entry<String, PostalAddress> record : result.entrySet()){
//                System.out.println(record.getKey());
//            }
//        } catch (Exception e) {
//
//            Assert.fail(e.toString());
//        }

    }

}