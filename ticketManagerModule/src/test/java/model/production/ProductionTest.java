package model.production;

import model.Performance;
import model.Production;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Performs unit tests on a {@link Production} object
 */
class ProductionTest {

    private final String PRODUCTION_NAME = "Production1";
    private final Performance PERFORMANCE = new Performance();
    private final ArrayList<Performance> PERFORMANCES = new ArrayList<Performance>() {{
        add(PERFORMANCE);
    }};

    private Production uut;

    @BeforeEach
    public void setUp() {

        Production temp = new Production();
        temp.setDisplayName(PRODUCTION_NAME);
        temp.setPerformances(PERFORMANCES);
        this.uut = temp;

    }

    /**
     * Tests that setting the name of the model.performance.production functions properly
     */
    @Test
    public void setProductionName() {
        String name = PRODUCTION_NAME + "Altered";
        uut.setDisplayName(name);
        String newName = uut.getDisplayName();
        Assert.assertEquals("setProductionName did not properly update the name: " + newName, name, newName);
    }

    /**
     * Tests that setting the performances properly updates the performances
     */
    @Test
    public void setPerformances() {
        Performance performance = new Performance();
        performance.setDisplayName("Title");
        ArrayList<Performance> performances = PERFORMANCES;
        performances.add(performance);
        uut.setPerformances(performances);
        ArrayList<Performance> newPerformances = uut.getPerformances();
        Assert.assertEquals("setPerformances did not peroperly update the performances", performances, newPerformances);
    }

    /**
     * Tests that getPerformances returns the correct ArrayList of performances
     */
    @Test
    public void getPerformances() {
        ArrayList<Performance> performances = uut.getPerformances();
        Assert.assertEquals("getPerformances did not return the correct performances", PERFORMANCES, performances);
    }

    /**
     * Tests that getProductionName functions properly
     */
    @Test
    public void getProductionName() {
        String name = uut.getDisplayName();
        Assert.assertEquals("getProductionName returned an unexpected value: " + name, PRODUCTION_NAME, name);
    }

    /**
     *Tests that add model.performance adds a new model.performance properly
     */
    @Test
    public void addPerformance() {
        Performance performance = new Performance();
        performance.setDisplayName("brand new model.performance");
        UUID uuid = performance.getUuid();
        uut.addExistingPerformance(performance);
        Performance newPerformance = uut.getPerformance(uuid);
        Assert.assertEquals("The model.performance added was not found or not identical", performance, newPerformance);
    }

    /**
     * Ensures that performances are removed from the ArrayList
     */
    @Test
    public void removePerformance() {
        Performance performance = new Performance();
        performance.setDisplayName("brand new model.performance");
        UUID uuid = performance.getUuid();
        uut.addExistingPerformance(performance);
        Assert.assertTrue("removePerformance did not successfully remove the model.performance", uut.removePerformance(uuid));
        Assert.assertNull("getPerformance should have returned null", uut.getPerformance(uuid));
    }

    /**
     * Ensures that getPerformance returns the expected value
     */
    @Test
    public void getPerformance() {
        UUID uuid = PERFORMANCE.getUuid();

        Performance performance = uut.getPerformance(PERFORMANCE.getUuid());
        Assert.assertEquals("getPerformance did not return the expected performande", PERFORMANCE, performance);
    }

}