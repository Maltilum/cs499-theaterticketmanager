package model.ticketHolders;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Test class for the {@link SeasonTicketHolder}
 */
class SeasonTicketHolderTest {
    private SeasonTicketHolder uut;
    private final String UUT_NAME = "SeasonTicketHolder1";
    private final float TICKET_PRICE = 40;
    private final boolean HAS_PAID = true;
    private final boolean IS_DISABLED = false;
    private final String DISABLED_NOTES = "N/A";
    private final String MISC_NOTES = "Miscellaneous";
    private final PostalAddress ADDRESS =  new PostalAddress("1555", "the", "City", "Nowhere", "090000");
    private final String PHONE_NUMBER = "3145551592";
    private final List<String> SEATING_PREFERENCES = new ArrayList<>();


    /**
     * Creates a new instance of the uut before each test
     */
    @BeforeEach
    public void setUp() {
        uut = getUut(UUT_NAME, TICKET_PRICE, HAS_PAID, IS_DISABLED, DISABLED_NOTES, MISC_NOTES, ADDRESS, PHONE_NUMBER, SEATING_PREFERENCES);
    }

    /**
     * Tests that the getAddress method works properly
     */
    @Test
    public void getAddress() {
        Assert.assertEquals("getAddress did not return the expected value" + uut.getAddress(), ADDRESS, uut.getAddress());
    }

    /**
     * Tests the setter method for a SeasonTicketHolder's address
     */
    @Test
    public void setAddress() {
        PostalAddress newAddress =  new PostalAddress("Our", "House", "City", "Midd Of", "The Street");
        uut.setAddress(newAddress);
        Assert.assertEquals("Address was not set properly" + uut.getAddress(), newAddress, uut.getAddress());
    }

    /**
     * Test method for getPhone_Number
     */
    @Test
    public void getPhone_number() {
        Assert.assertEquals("getPhone_Number did not return the expected value: " + uut.getPhone_number(), PHONE_NUMBER, uut.getPhone_number());
    }

    /**
     * Tests that setting a new phone number for a SeasonTicketHolder works
     */
    @Test
    public void setPhone_number() {
        String newNumber = "1235554567";
        uut.setPhone_number(newNumber);
        Assert.assertEquals("The new phone number was not successfully updated", newNumber, uut.getPhone_number());
    }

    /**
     * Tests whether the getSeating_preferences method functions properly
     */
    @Test
    public void getSeating_preferences() {
        List<String> preferences = uut.getSeating_preferences();
        Assert.assertEquals("getSeating_preference method returned unexpected value: " + preferences.toString(), SEATING_PREFERENCES, preferences);
    }

    /**
     * Tests the setSeating_preferences method changes the seating preferences properly
     */
    @Test
    public void setSeating_preferences() {
        String newPreference = "Big chair, comfy seat";
        List<String> newPreferences = new ArrayList<>();
        newPreferences.add(newPreference);
        uut.setSeating_preferences(newPreferences);
        List<String> returnedPreferences = uut.getSeating_preferences();
        Assert.assertEquals("getSeating_Preferences returned an unexpected value: " + returnedPreferences.toString(), newPreferences, returnedPreferences);
    }

    @Test
    public void testEquals() {
        SeasonTicketHolder copyTicketHolder = uut;
        Assert.assertEquals("Equals method is not functioning properly", uut, copyTicketHolder);
    }

    /**
     * Creates an instance of the Unit Under Test
     *
     * @param name                Name of the ticket holder.
     * @param ticket_price        The price they paid for the ticket.
     * @param has_paid            If they have paid for their ticket in advance or not.
     * @param is_disabled         If the person is disabled.
     * @param disabled_notes      Notes about their disability.
     * @param misc_notes          Other notes if needed
     * @param address             The holder's mailing address
     * @param phone_number        The holder's phone number
     * @param seating_preferences A list of strings that encode the season pass holder's seating preferences.
     * @return A {@link SeasonTicketHolder} object with the defined properties
     */
    private SeasonTicketHolder getUut(String name, float ticket_price, boolean has_paid, boolean is_disabled,
                                      String disabled_notes, String misc_notes, PostalAddress address, String phone_number, List<String> seating_preferences) {
        return new SeasonTicketHolder(name, ticket_price, has_paid, is_disabled, disabled_notes, misc_notes, address, phone_number, seating_preferences);
    }
}