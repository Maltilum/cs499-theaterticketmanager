package model.ticketHolders;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class TicketHolderListTest {

    private TicketHolderList uut;
    private List<TicketHolder> TICKET_HOLDER_LIST = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        this.uut = new TicketHolderList();
        this.uut.setTicketHolders(TICKET_HOLDER_LIST);
    }

    /**
     * Tests that the getTicketHolders method returns the expected value
     */
    @Test
    public void getTicketHolders() {
        List<TicketHolder> ticketHolders = uut.getTicketHolders();
        Assert.assertEquals("getTicketHolders did not return the expected value", TICKET_HOLDER_LIST, uut.getTicketHolders());
    }

    /**
     * Tests that setTicketHolders updates the ticket holder properly
     */
    @Test
    public void setTicketHolders() {
        List<TicketHolder> ticketHolders = TICKET_HOLDER_LIST;
        TicketHolder ticketHolder = new TicketHolder();
        ticketHolder.setDisplayName("TicketHolder1");
        ticketHolders.add(ticketHolder);
        Assert.assertEquals("setTicketHolders did not properly update the value: ", ticketHolders, uut.getTicketHolders());
    }

    /**
     * Tests that SeasonTicketHolders are added to the TicketHolderList
     */
    @Test
    public void addSeaTicketHold() {
        PostalAddress address = new PostalAddress("1555", "the", "City", "Nowhere", "090000");
        SeasonTicketHolder seasonTicketHolder = new SeasonTicketHolder("SeasonTicketHolder1", (float) 30, true, false, "N/A", "None", address, "phoneNumber", null);
        uut.addSeaTicketHold(seasonTicketHolder);
        List<TicketHolder> ticketHolders = uut.getTicketHolders();
        Assert.assertTrue("addSeaTicketHold did not add the SeasonTicketHolder", ticketHolders.contains(seasonTicketHolder));
    }

}