package model.ticketHolders;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TicketHolderTest {
    private final String NAME = "TicketHolder1";
    private final float TICKET_PRICE = 40;
    private final boolean PAID = true;
    private final boolean DISABLED = false;
    private final String DISABLED_NOTES = "N/A";
    private final String MISC_NOTES = "Empty";

    private TicketHolder uut;

    @BeforeEach
    public void setUp() {
        uut = getUut(NAME, TICKET_PRICE, PAID, DISABLED, DISABLED_NOTES, MISC_NOTES);
    }

    /**
     * Tests that the UUID of the TicketHolder is not null
     */
    @Test
    public void getUuid() {
        Assert.assertNotNull("UUID for TicketHolder was null", uut.getUuid());
    }

    /**
     * Tests that the getName method functions properly
     */
    @Test
    public void getName() {
        String returnedName = uut.getDisplayName();
        Assert.assertEquals("getName method returned unexpected result: " + returnedName, NAME, returnedName);
    }

    /**
     * Tests that the setName method properly changes the name
     */
    @Test
    public void setName() {
        String newName = "newName";
        uut.setDisplayName(newName);
        String returnedName = uut.getDisplayName();
        Assert.assertEquals("setName did not set name to expected value: " + returnedName, newName, returnedName);
    }

    /**
     * Tests that getTicket_price functions properly
     */
    @Test
    public void getTicket_price() {
        float ticketPrice = uut.getTicketPrice();
        Assert.assertEquals("getTicket_price did not return expected result: " + ticketPrice, TICKET_PRICE, ticketPrice, 0.0);
    }

    /**
     * Tests that setTicket_Price functions properly
     */
    @Test
    public void setTicket_price() {
        float newPrice = TICKET_PRICE - (float) 10;
        uut.setTicketPrice(newPrice);
        float returnedPrice = uut.getTicketPrice();
        Assert.assertEquals("setTicket_price did not set the price to the expected value: " + returnedPrice, newPrice, returnedPrice, 0.0);
    }

    /**
     * Tests functionality of the getPaid method
     */
    @Test
    public void getPaid() {
        boolean paid = uut.getPaid();
        Assert.assertEquals("getPaid did not return the expected value: " + paid, PAID, paid);
    }

    /**
     * Tests whether setPaid properly updates the value
     */
    @Test
    public void setPaid() {
        boolean hasPaid = !PAID;
        uut.setPaid(hasPaid);
        boolean returnedPaid = uut.getPaid();
        Assert.assertEquals("setPaid did not properly set the value: " + returnedPaid, hasPaid, returnedPaid);
    }

    /**
     * Tests whether getDisabled functions properly
     */
    @Test
    public void getDisabled() {
        boolean isDisabled = uut.getDisabled();
        Assert.assertEquals("getDisabled did not return the expected result: " + isDisabled, DISABLED, isDisabled);
    }

    /**
     * Tests whether setDisabled properly updates whether the ticketHolder is disabled
     */
    @Test
    public void setDisabled() {
        boolean isDisabled = !DISABLED;
        uut.setDisabled(isDisabled);
        boolean returnedDisabled = uut.getDisabled();
        Assert.assertEquals("setDisabled did not properly update the value: " + returnedDisabled, isDisabled, returnedDisabled);
    }

    /**
     * Tests whether the getDisabled_notes method functions as expected
     */
    @Test
    public void getDisabled_notes() {
        String disabledNotes = uut.getDisabledNotes();
        Assert.assertEquals("getDisabled_notes did not return the expected value: " + disabledNotes, DISABLED_NOTES, disabledNotes);
    }

    /**
     * Tests whether the the setDisabled_notes functions properly
     */
    @Test
    public void setDisabled_notes() {
        String disabledNotes = "new notes";
        uut.setDisabledNotes(disabledNotes);
        String returnedNotes = uut.getDisabledNotes();
        Assert.assertEquals("setDisabled_notes did not properly update the value: " + returnedNotes, disabledNotes, returnedNotes);
    }

    /**
     * Tests whether the getMisc_notes returns the expected value
     */
    @Test
    public void getMisc_notes() {
        String miscNotes = uut.getMiscNotes();
        Assert.assertEquals("getMisc_notes did not return the expected value: " + miscNotes, MISC_NOTES, miscNotes);
    }

    /**
     * Tests whether setMisc_notes properly updates the miscellaneous notes
     */
    @Test
    public void setMisc_notes() {
        String miscNotes = "Here's the new notes";
        uut.setMiscNotes(miscNotes);
        String returnedNotes = uut.getMiscNotes();
        Assert.assertEquals("setMisc_notes did not properly update the value: " + returnedNotes, miscNotes, returnedNotes);
    }

    /**
     * Tests whether the equals method properly assesses whether TicketHolders are the same
     */
    @Test
    public void testEquals() {
        TicketHolder copyTicketHolder = uut;
        Assert.assertEquals("Equals method returned false for equal TicketHolders", uut, copyTicketHolder);
    }

    /**
     * Creates a unit under test with the values specified by parameters
     *
     * @param name          Name of the ticket holder.
     * @param ticketPrice   The price they paid for the ticket.
     * @param paid          If they have paid for their ticket in advance or not.
     * @param disabled      If the person is disabled.
     * @param disabledNotes Notes about their disability.
     * @param misc_notes    Other notes if needed
     * @return The unit under test
     */
    private TicketHolder getUut(String name, float ticketPrice, boolean paid, boolean disabled, String disabledNotes, String misc_notes) {
        return new TicketHolder(name, ticketPrice, paid, disabled, disabledNotes, misc_notes);
    }
}