package presenters;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import presenters.response.BasicResponse;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;

import java.io.StringReader;
import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

class BasicPresenterTest {

    @Test
    public void TestRequest(){
        BasicPresenter presenter;
        try {
            presenter = new BasicPresenter();

            presenter.root.createNewSeason("testSeason", Calendar.getInstance().getTime(), Calendar.getInstance().getTime());
            presenter.root.createNewSeason("Cats3ThisTimeIt'sPersonal", Calendar.getInstance().getTime(), Calendar.getInstance().getTime());


            String testRequestString ="<?xml version=\"1.0\"?>\n<request type = 'GET'>\n    <query>//Season</query>\n</request>\n";
            JAXBContext responseContext = JAXBContext.newInstance(BasicResponse.class);
            Unmarshaller respUnmarshaler = responseContext.createUnmarshaller();
            StringReader reader = new StringReader(presenter.request(testRequestString));
            BasicResponse basicResponse = (BasicResponse)respUnmarshaler.unmarshal(reader);


        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.toString());
            return;
        }








    }

}