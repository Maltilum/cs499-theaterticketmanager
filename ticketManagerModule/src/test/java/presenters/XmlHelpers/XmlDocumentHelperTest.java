package presenters.XmlHelpers;

import model.Season;
import model.dataHelpers.BasicSaveStructure;
import model.dataHelpers.XMLFactory;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBContext;

import java.util.ArrayList;
import java.util.List;

class XmlDocumentHelperTest {

    @Test
    public void Test_StringToDoc(){
        Season season = new Season();
        season.setDisplayName("The first");
        season.addNewProduction("test");
        season.addNewProduction("hello");

        Season season1 = new Season();
        season1.setDisplayName("The Last Year");

        season1.addNewProduction("The man who sold the world.");

        BasicSaveStructure basicSaveStructure = new BasicSaveStructure();

        List<Season> seasons = new ArrayList<>();

        seasons.add(season);
        seasons.add(season1);

        basicSaveStructure.setSeasonsList(seasons);


        XMLFactory xmlFactory = new XMLFactory();

        try {
            String xmlString = xmlFactory.convertAnyObject_Full(basicSaveStructure, JAXBContext.newInstance(BasicSaveStructure.class));

            XmlDocumentHelper documentBuilder = new XmlDocumentHelper();
            NodeList nodeList = documentBuilder.queryXmlString(xmlString, "//Season");

            for(int i = 0; i < nodeList.getLength(); i++){
                Node node = nodeList.item(i);
                String nodeString = documentBuilder.nodeToXmlString(node);

                System.out.println(nodeString);
            }







           // System.out.println(doc);





        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.toString());
        }
    }

}