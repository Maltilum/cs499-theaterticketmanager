package row;

import model.Row;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import model.Seat;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Test class for the {@link Row} class
 */
class RowTest {
    private static final float ROW_PRICE = 40;
    private Row uut;
    private final String ROW_NAME = "Row1";

    @BeforeEach
    public void setUp() {
        this.uut = getUut(ROW_NAME, ROW_PRICE, new ArrayList<>());
    }

    /**
     * Method to test the getRowName method in Row
     */
    @Test
    public void getRowName() {
        String nameReceived = uut.getDisplayName();
        Assert.assertEquals("getRowName did not return the expected name" + nameReceived, ROW_NAME, nameReceived);
    }

    /**
     * Method to test the setRowName method in Row
     */
    @Test
    public void setRowName() {
        String newName = "newName";
        uut.setDisplayName(newName);
        String nameReceived = uut.getDisplayName();
        Assert.assertEquals("setRowName did not properly update the name", newName, nameReceived);
    }

    /**
     * Method to test the getPrice method in Row
     */
    @Test
    public void getPrice() {
        float priceReceived = uut.getPrice();
        Assert.assertEquals("getRowName did not return the expected name" + priceReceived, ROW_PRICE, priceReceived, 0.0);
    }

    /**
     * Method to test the setPrice method in Row
     */
    @Test
    public void setPrice() {
        float newPrice = 20;
        uut.setPrice(newPrice);
        float priceReceived = uut.getPrice();
        Assert.assertEquals("setPrice did not properly update the price: "+ priceReceived, newPrice, priceReceived, 0.0);
    }

    /**
     * Tests that the UUID of a row is properly set and the getter method works
     */
    @Test
    public void getUuid() {
        Assert.assertNotNull("getUuid for the Row null", uut.getUuid());
    }

    /**
     * Tests that setting and getting an ArrayList of seats works properly
     */
    @Test
    public void testGetAndSetSeats() {
        ArrayList<Seat> seats = new ArrayList<>();
        seats.add(new Seat());
        seats.add(new Seat());
        uut.setSeats(seats);

        Assert.assertEquals("getSeats did not return the value that the uut was given", seats, uut.getSeats());
    }

    /**
     * Tests that a seat added to and gotten from
     */
    @Test
    public void testAddAndGetSeat() {
        Seat seat = new Seat();
        uut.addExistingSeat(seat);
        UUID seatUuid = seat.getUuid();
        Seat newSeat = uut.getSeat(seatUuid);

        Assert.assertEquals("Seat added to row was not returned:" + newSeat.getUuid(), seat, newSeat);
    }

    /**
     * Tests that seats are properly removed from a row
     */
    @Test
    public void removeSeat() {
        Seat seat = new Seat();
        UUID seatUuid = seat.getUuid();
        uut.addExistingSeat(seat);

        Assert.assertTrue("removeSeat method returned false.", uut.removeSeat(seatUuid));
        Assert.assertNull("Seat was not removed from the Row", uut.getSeat(seatUuid));
    }

    /**
     * Generates a Row with predefined values.
     *
     * @param name  The predefined name of the Row
     * @param price The predefined price of the row
     * @param seats The predefined ArrayList of Seats in the row
     * @return The Row with specified values
     */
    private Row getUut(String name, float price, ArrayList<Seat> seats) {
        return new Row(name, price, seats);
    }

    /**
     * Test to make sure the equals method is functioning properly
     */
    @Test
    void testEquals() {
        Row copy = uut;
        Assert.assertTrue("Overridden equals method returned false when true was expected", uut.equals(copy));
        copy.setDisplayName("Changed");
        Assert.assertTrue("Overridden equals method returned true when false was expected", uut.equals(copy));
    }

}