package section;

import model.Section;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import model.Row;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Test class for the {@link Section} class
 */
class SectionTest {
    private final boolean DEFAULT_AVAILABILITY = true;
    private final float DEFAULT_PRICE = 50;
    private final String DEFAULT_NAME = "Section1";
    private final ArrayList<Row> DEFAULT_ROWS = new ArrayList<>();

    private Section uut;

    @BeforeEach
    public void setUp() {
        this.uut = getUut(DEFAULT_AVAILABILITY, DEFAULT_PRICE, DEFAULT_NAME, DEFAULT_ROWS);
    }

    /**
     * Tests whether the getter for the Section availability is functioning properly
     */
    @Test
    public void isSectionAvailable() {
        Assert.assertEquals("Getter for Section availability returned the wrong value.", DEFAULT_AVAILABILITY, uut.isSectionAvailable());
    }

    /**
     * Tests the setter for Section availability
     */
    @Test
    public void setSectionAvailable() {
        boolean newAvailability = false;
        uut.setSectionAvailable(newAvailability);
        Assert.assertEquals("Setter for Section availability returned the wrong value.", newAvailability, uut.isSectionAvailable());
    }

    /**
     * Tests the getter for the Section availability
     */
    @Test
    public void getSectionPrice() {
        Assert.assertEquals("Getter for Section price returned the wrong value.", DEFAULT_PRICE, uut.getSectionPrice(), 0.0);
    }

    /**
     * Tests setting the price of the Section
     */
    @Test
    public void setSectionPrice() {
        float newSectionPrice = 15;
        uut.setSectionPrice(newSectionPrice);
        Assert.assertEquals("Setter for the Section price did not set the right value", newSectionPrice, uut.getSectionPrice(), 0.0);
    }

    /**
     * Tests the getter for the Section name
     */
    @Test
    public void getSectionName() {
        Assert.assertEquals("getSectionName did not return expected value", DEFAULT_NAME, uut.getDisplayName());
    }

    /**
     * Tests the setter for the section name
     */
    @Test
    public void setSectionName() {
        String newName = "new_name";
        uut.setDisplayName(newName);
        Assert.assertEquals("setSectionName did not set the correct name.", newName, uut.getDisplayName());
    }

    /**
     * Tests the getUuid method
     */
    @Test
    public void getUuid() {
        Assert.assertNotNull("UUID for Section was null", uut.getUuid());
    }

    /**
     * Tests that the getRows and setRows methods are functioning properly
     */
    @Test
    public void testGetAndSetRows() {
        ArrayList<Row> rows = new ArrayList<>();
        rows.add(new Row());
        rows.add(new Row());
        uut.setRows(rows);

        Assert.assertEquals("Rows given to Section not same as returned.", rows, uut.getRows());
    }

    /**
     * Tests that the addRow and GetRow methods function properly
     */
    @Test
    public void testAddAndGetRow() {
        Row row = new Row();
        UUID rowUuid = row.getUuid();
        uut.addExistingRow(row);

        Assert.assertEquals("Row given to Section was not the same as the Row received", row, uut.getRow(rowUuid));
    }

    /**
     * Tests that removeRow functions properly
     */
    @Test
    public void removeRow() {
        Row row = new Row();
        UUID rowUuid = row.getUuid();
        uut.addExistingRow(row);

        Assert.assertTrue("removeRow method returned false", uut.removeRow(rowUuid));
        Assert.assertNull("Row was not removed from Section", uut.getRow(rowUuid));
    }

    /**
     * Creates a Unit Under Test with specified values
     *
     * @param sectionAvailable The specified Section availability
     * @param sectionPrice     The specified price of the Section
     * @param sectionName      The specified name of the Section
     * @param rows             The specified ArrayList of Rows in the Section
     * @return The Section with specified values
     */
    private Section getUut(boolean sectionAvailable, float sectionPrice, String sectionName, ArrayList<Row> rows) {
        return new Section(sectionAvailable, sectionPrice, sectionName, rows);
    }

    /**
     * Test to make sure the equals method is functioning properly
     */
    @Test
    void testEquals() {
        Section copy = uut;
        Assert.assertTrue("Overridden equals method returned false when true was expected", uut.equals(copy));
        copy.setDisplayName("Changed");
        Assert.assertTrue("Overridden equals method returned true when false was expected", uut.equals(copy));
    }
}