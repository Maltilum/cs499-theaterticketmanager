package view.xmlUtils;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class XmlUtilsTest {

    @Test
    public void XmlToObject_Example() {
        //A stand-in for a response string from a presenter. It contains the info for a Season Ticker Holder. Below is the data in
        //Readable form.
        /*
            <?xml version="1.0"?>
            <response>
                <uuid>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</uuid>
                <ticketPrice>42.42</ticketPrice>
                <paid>false</paid>
                <disabled>true</disabled>
                <miscNotes>This man is a vampire.</miscNotes>
                <phone_number>555-555-5555</phone_number>
            </response>
         */
        String TICKET_HOLDER_EXAMPLE_STRING = "<?xml version=\"1.0\"?>\n<response>\n    <uuid>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</uuid>\n    <ticketPrice>42.42</ticketPrice>\n    <paid>false</paid>\n    <disabled>true</disabled>\n    <miscNotes>This man is a vampire.</miscNotes>\n    <phone_number>555-555-5555</phone_number>\n</response>\n";

        /*Here we use XmlUtils to unmarshal the string. Unmarshal meaning to
        convert the string into an object.

        We need to provide the class context to the utils function for it to work.
         */
        STicketDataHolder exampleDataHolder = XmlUtils.unmarshall(TICKET_HOLDER_EXAMPLE_STRING, STicketDataHolder.class);


        /*
        From this point onward, you can access data in the DataHolder object like normal.
         */
        Assert.assertEquals("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", exampleDataHolder.uuid);
        Assert.assertEquals("42.42", exampleDataHolder.ticketPrice);
        Assert.assertEquals(false, exampleDataHolder.paid);
        Assert.assertEquals(true, exampleDataHolder.disabled);
        Assert.assertEquals("This man is a vampire.", exampleDataHolder.miscNotes);
        Assert.assertEquals("555-555-5555", exampleDataHolder.phone_number);

    }

    @Test
    public void XmlToObject_Test() {
        //A constant string for testing things.
        String xmlTestString = "<?xml version=\"1.0\"?>\n<response>\n    <testString>This</testString>\n</response>";

        //A test holder class for examples
        XmlTestHolder testHolder = new XmlTestHolder();

        try {
            //Unmarshall an xml string into a data holder.
            testHolder = (XmlTestHolder) XmlUtils.unmarshall(xmlTestString, XmlTestHolder.class);
            Assert.assertEquals("This", testHolder.testString);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.toString());
        }
    }

}